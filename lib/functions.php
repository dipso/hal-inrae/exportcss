<?php


/*** fonctions basics ****/
//////////////////////////
function moveFile($dossierSource , $dossierDestination){
    $retour = 1; 
    if(!file_exists($dossierSource)) { 
		$retour = -1; 
    } else { 
		if(!copy($dossierSource, $dossierDestination)) { 
			$retour = -2; 
		} else { 
			if(!unlink($dossierSource)) { 
				$retour = -3; 
			} 
		} 
    } 
    return($retour);
}


/************************************************************/
// Fonctions permettant l'affichage des 19 types de document
/************************************************************/

function changeUri($notice) {
	// L’url d’origine : https://hal-univ-lyon1.archives-ouvertes.fr/hal-02016398
	// Devrait être : https://hal.inrae.fr/hal-0201639 ou https://hal.science/hal-03858064 si coolCode_s ne contient pas INRAE	
	if (in_array("INRAE",$notice['collCode_s']))
		return "https://hal.inrae.fr/".$notice['halId_s'];
	else
		return "https://hal.science/".$notice['halId_s'];	
}


function changeDoi($doi){
	$lien = "https://dx.doi.org/".$doi;
	return $lien;
}

function getSansEm($string,&$sectionRtf){
	$pos = strpos("<em>", $string);
	$string = htmlspecialchars($string); 
	$tab = explode("&lt;em&gt;",$string);
	if (isset($tab[0])) {  // avant la balise = pas en italic
		$sectionRtf->writeText(htmlspecialchars($tab[0]));
		$tab2 = explode('&lt;/em&gt;',$tab[1]);   //$tab[1] contient tout ce qu'il y a apres <em>
		if (isset($tab2[0]))
			$sectionRtf->writeText(htmlspecialchars($tab2[0]));
		if (isset($tab2[1]))
			$sectionRtf->writeText(htmlspecialchars($tab2[1]));
			
		$sectionRtf->writeText(". ");
	}
}

function code_to_country( $code ){
    $code = strtoupper($code);
    $countryList = array(
	'EN' => 'Anglais',
    'AF' => 'Afghanistan',
    'ZA' => 'Afrique du Sud',
    'AL' => 'Albanie',
    'DZ' => 'Algérie',
    'DE' => 'Allemagne',
    'AD' => 'Andorre',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AQ' => 'Antarctique',
    'AG' => 'Antigua-et-Barbuda',
    'SA' => 'Arabie saoudite',
    'AR' => 'Argentine',
    'AM' => 'Arménie',
    'AW' => 'Aruba',
    'AU' => 'Australie',
    'AT' => 'Autriche',
    'AZ' => 'Azerbaïdjan',
    'BS' => 'Bahamas',
    'BH' => 'Bahreïn',
    'BD' => 'Bangladesh',
    'BB' => 'Barbade',
    'BE' => 'Belgique',
    'BZ' => 'Belize',
    'BJ' => 'Bénin',
    'BM' => 'Bermudes',
    'BT' => 'Bhoutan',
    'BY' => 'Biélorussie',
    'BO' => 'Bolivie',
    'BA' => 'Bosnie-Herzégovine',
    'BW' => 'Botswana',
    'BR' => 'Brésil',
    'BN' => 'Brunei',
    'BG' => 'Bulgarie',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodge',
    'CM' => 'Cameroun',
    'CA' => 'Canada',
    'CV' => 'Cap-Vert',
    'CL' => 'Chili',
    'CN' => 'Chine',
    'CY' => 'Chypre',
    'CO' => 'Colombie',
    'KM' => 'Comores',
    'CG' => 'Congo-Brazzaville',
    'CD' => 'Congo-Kinshasa',
    'KP' => 'Corée du Nord',
    'KR' => 'Corée du Sud',
    'CR' => 'Costa Rica',
    'CI' => 'Côte d’Ivoire',
    'HR' => 'Croatie',
    'CU' => 'Cuba',
    'CW' => 'Curaçao',
    'DK' => 'Danemark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominique',
    'EG' => 'Égypte',
    'AE' => 'Émirats arabes unis',
    'EC' => 'Équateur',
    'ER' => 'Érythrée',
    'ES' => 'Espagne',
    'EE' => 'Estonie',
    'SZ' => 'Eswatini',
    'VA' => 'État de la Cité du Vatican',
    'US' => 'États-Unis',
    'ET' => 'Éthiopie',
    'FJ' => 'Fidji',
    'FI' => 'Finlande',
    'FR' => 'France',
    'GA' => 'Gabon',
    'GM' => 'Gambie',
    'GE' => 'Géorgie',
    'GS' => 'Géorgie du Sud-et-les Îles Sandwich du Sud',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Grèce',
    'GD' => 'Grenade',
    'GL' => 'Groenland',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernesey',
    'GN' => 'Guinée',
    'GQ' => 'Guinée équatoriale',
    'GW' => 'Guinée-Bissau',
    'GY' => 'Guyana',
    'GF' => 'Guyane française',
    'HT' => 'Haïti',
    'HN' => 'Honduras',
    'HU' => 'Hongrie',
    'BV' => 'Île Bouvet',
    'CX' => 'Île Christmas',
    'IM' => 'Île de Man',
    'NF' => 'Île Norfolk',
    'AX' => 'Îles Åland',
    'KY' => 'Îles Caïmans',
    'CC' => 'Îles Cocos',
    'CK' => 'Îles Cook',
    'FO' => 'Îles Féroé',
    'HM' => 'Îles Heard-et-MacDonald',
    'FK' => 'Îles Malouines',
    'MP' => 'Îles Mariannes du Nord',
    'MH' => 'Îles Marshall',
    'UM' => 'Îles mineures éloignées des États-Unis',
    'PN' => 'Îles Pitcairn',
    'SB' => 'Îles Salomon',
    'TC' => 'Îles Turques-et-Caïques',
    'VG' => 'Îles Vierges britanniques',
    'VI' => 'Îles Vierges des États-Unis',
    'IN' => 'Inde',
    'ID' => 'Indonésie',
    'IQ' => 'Irak',
    'IR' => 'Iran',
    'IE' => 'Irlande',
    'IS' => 'Islande',
    'IL' => 'Israël',
    'IT' => 'Italie',
    'JM' => 'Jamaïque',
    'JP' => 'Japon',
    'JE' => 'Jersey',
    'JO' => 'Jordanie',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KG' => 'Kirghizstan',
    'KI' => 'Kiribati',
    'KW' => 'Koweït',
    'RE' => 'La Réunion',
    'LA' => 'Laos',
    'LS' => 'Lesotho',
    'LV' => 'Lettonie',
    'LB' => 'Liban',
    'LR' => 'Liberia',
    'LY' => 'Libye',
    'LI' => 'Liechtenstein',
    'LT' => 'Lituanie',
    'LU' => 'Luxembourg',
    'MK' => 'Macédoine du Nord',
    'MG' => 'Madagascar',
    'MY' => 'Malaisie',
    'MW' => 'Malawi',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malte',
    'MA' => 'Maroc',
    'MQ' => 'Martinique',
    'MU' => 'Maurice',
    'MR' => 'Mauritanie',
    'YT' => 'Mayotte',
    'MX' => 'Mexique',
    'FM' => 'Micronésie',
    'MD' => 'Moldavie',
    'MC' => 'Monaco',
    'MN' => 'Mongolie',
    'ME' => 'Monténégro',
    'MS' => 'Montserrat',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar (Birmanie)',
    'NA' => 'Namibie',
    'NR' => 'Nauru',
    'NP' => 'Népal',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NO' => 'Norvège',
    'NC' => 'Nouvelle-Calédonie',
    'NZ' => 'Nouvelle-Zélande',
    'OM' => 'Oman',
    'UG' => 'Ouganda',
    'UZ' => 'Ouzbékistan',
    'PK' => 'Pakistan',
    'PW' => 'Palaos',
    'PA' => 'Panama',
    'PG' => 'Papouasie-Nouvelle-Guinée',
    'PY' => 'Paraguay',
    'NL' => 'Pays-Bas',
    'BQ' => 'Pays-Bas caribéens',
    'PE' => 'Pérou',
    'PH' => 'Philippines',
    'PL' => 'Pologne',
    'PF' => 'Polynésie française',
    'PR' => 'Porto Rico',
    'PT' => 'Portugal',
    'QA' => 'Qatar',
    'HK' => 'R.A.S. chinoise de Hong Kong',
    'MO' => 'R.A.S. chinoise de Macao',
    'CF' => 'République centrafricaine',
    'DO' => 'République dominicaine',
    'RO' => 'Roumanie',
    'GB' => 'Royaume-Uni',
    'RU' => 'Russie',
    'RW' => 'Rwanda',
    'EH' => 'Sahara occidental',
    'BL' => 'Saint-Barthélemy',
    'KN' => 'Saint-Christophe-et-Niévès',
    'SM' => 'Saint-Marin',
    'MF' => 'Saint-Martin',
    'SX' => 'Saint-Martin (partie néerlandaise)',
    'PM' => 'Saint-Pierre-et-Miquelon',
    'VC' => 'Saint-Vincent-et-les Grenadines',
    'SH' => 'Sainte-Hélène',
    'LC' => 'Sainte-Lucie',
    'SV' => 'Salvador',
    'WS' => 'Samoa',
    'AS' => 'Samoa américaines',
    'ST' => 'Sao Tomé-et-Principe',
    'SN' => 'Sénégal',
    'RS' => 'Serbie',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapour',
    'SK' => 'Slovaquie',
    'SI' => 'Slovénie',
    'SO' => 'Somalie',
    'SD' => 'Soudan',
    'SS' => 'Soudan du Sud',
    'LK' => 'Sri Lanka',
    'SE' => 'Suède',
    'CH' => 'Suisse',
    'SR' => 'Suriname',
    'SJ' => 'Svalbard et Jan Mayen',
    'SY' => 'Syrie',
    'TJ' => 'Tadjikistan',
    'TW' => 'Taïwan',
    'TZ' => 'Tanzanie',
    'TD' => 'Tchad',
    'CZ' => 'Tchéquie',
    'TF' => 'Terres australes françaises',
    'IO' => 'Territoire britannique de l’océan Indien',
    'PS' => 'Territoires palestiniens',
    'TH' => 'Thaïlande',
    'TL' => 'Timor oriental',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinité-et-Tobago',
    'TN' => 'Tunisie',
    'TM' => 'Turkménistan',
    'TR' => 'Turquie',
    'TV' => 'Tuvalu',
    'UA' => 'Ukraine',
    'UY' => 'Uruguay',
    'VU' => 'Vanuatu',
    'VE' => 'Venezuela',
    'VN' => 'Viêt Nam',
    'WF' => 'Wallis-et-Futuna',
    'YE' => 'Yémen',
    'ZM' => 'Zambie',
    'ZW' => 'Zimbabwe',
    );

    if ( !$countryList[$code] )
		return $code;
    else
		return $countryList[$code];
}

function getPublicVise($code){
	switch($code){
		case "SC" : return  "Scientifique"; 		break;
		case "TE" : return  "Technique"; 			break;
		case "PP" : return  "Pouvoirs publics"; 	break;
		case "ET" : return  "Etudiants"; 			break;
		case "GP" : return  "Grand public"; 		break;
		case "AU" : return  "Autre"; 				break;
	}
}


// correspondance pour le champ lectureType_s
function getLectureType($code){
	switch($code){
		case 1 :  return  "DEA"; 									break;
		case 2 :  return  "École thématique"; 						break;
		case 7 :  return  "3ème cycle"; 							break;
		case 10 : return  "École d'ingénieur"; 						break;
		case 11 : return  "Licence"; 								break;
		case 12 : return  "Master"; 								break;
		case 13 : return  "Doctorat"; 								break;
		case 14 : return  "DEUG"; 									break;
		case 15 : return  "Maîtrise"; 								break;
		case 21 : return  "Licence/L1"; 							break;
		case 22 : return  "Licence/L2"; 							break;
		case 23 : return  "Licence/L3"; 							break;
		case 31 : return  "Master/M1"; 								break;
		case 32 : return  "Master/M2"; 								break;
		case 40 : return  "Vulgarisation"; 							break;
	}
}

//    "inra_otherType_Comm_local_BP" => "Préface d'acte",
function ssTypeComm($code){
	switch($code){
		case "FT" : return  "Full paper"; 			break;
		case "ST" : return  "Short paper"; 			break;
		case "LA" : return  "Extended abstract"; 	break;
		case "AB" : return  "Abstract"; 			break;
		case "OP" : return  "Présentation orale"; 	break;
		case "BP" : return  "Préface d'acte"; 		break;
	}
}
//    "inra_otherType_Other_local_BD" => "Bande dessinée",
function ssTypeOther($code){
	switch($code){
		case "BL" : return  "Article de blog / Web"; 						break;
		case "CR" : return  "Compte-rendu d'ouvrage ou Note de lecture"; 	break;
		case "NO" : return  "Notice d'encyclopédie ou de dictionnaire"; 	break;
		case "TR" : return  "Traduction"; 									break;
		case "IL" : return  "Illustration"; 								break;
		case "CH" : return  "Charte"; 										break;
		case "NL" : return  "Newsletter"; 									break;
		case "LG" : return  "Livret guide"; 								break;
		case "CP" : return  "Communiqué de presse"; 						break;
		case "RE" : return  "Récompense"; 									break;
		case "PF" : return  "Plaquette / Flyer"; 							break;
		case "PQ" : return  "Plan qualité"; 								break;
		case "CN" : return  "Certification / Norme"; 						break;
		case "LB" : return  "Livre blanc"; 									break;
		case "NC" : return  "Note de cadrage"; 								break;
		case "FT" : return  "Fiche technique"; 								break;
		case "PM" : return  "Presse / Média"; 								break;
		case "BD" : return  "Bande dessinée"; 								break;
	}
}
//    "inra_developmentType_local_CG" => "Couche graphique",
function ssTypeDevelopment($code){
	switch($code){
		case "BD" : return  "Base de données"; 			break;
		case "VO" : return  "Vocabulaire"; 				break;
		case "LB" : return  "Librairie"; 				break;
		case "PL" : return  "Plugin"; 					break;
		case "SS" : return  "Système scientifique"; 	break;
		case "SW" : return  "Site web"; 				break;
		case "SG" : return  "SIG"; 						break;
		case "CG" : return  "Couche graphique"; 		break;
	}
}

function getTypeDoc($code){
	switch($code){
		case 'ART':		  return "Article";								break;
		case 'REPORT':    return "Rapport";    							break;
		case 'OUV':		  return "Ouvrage";								break;
		case 'DOUV':      return "Direction d'ouvrage ou d'actes";		break;
		case 'COUV':      return "Chapitre d'ouvrage";					break;
		case 'COMM':      return "Communication dans un congrès";		break;
		case 'HDR':		  return "HDR";									break;					
		case 'OTHER':	  return "Autre publication scientifique";		break;  // Autres devient Autre publication scientifique  12/2022
		case 'SOFTWARE':  return "Logiciel";							break;
		case 'PATENT':    return "Brevet";								break;
		case 'LECTURE':   return "Cours";								break;
		case 'VIDEO':     return "Vidéo";								break;
		case 'MAP':       return "Carte";								break;
		case 'IMG':       return "Image";								break;
		case 'SON':       return "Son";									break;
		case 'POSTER':    return "Poster";								break;
		case 'MEM':       return "Mémoire d'étudiant";					break;
		case 'CREPORT':   return "Chapitre de rapport";	break;
		case 'THESE':     return "Thèse";								break;
		case 'UNDEFINED': return "Pré-publication";						break;
		/* Nouveaux types 12/2022 */
		case 'MEMLIC':      return "Mémoire de licence";						break;
		case 'PROCEEDINGS': return "Proceedings / recueil de communications";  	break;
		case 'ISSUE':	    return "N° spécial de revue / special issue";      	break;
		case 'TRAD':	    return "Traduction";								break;
		case 'BLOG':	    return "Article de blog scientifique";				break;
		case 'NOTICE':		return "Notice d'encyclopédie ou de dictionnaire";	break;
		/* types de la section Non classé  */
		case 'PRESCONF':    return "Document associé à des manifestations scientifiques";  break;
		case 'ETABTHESE':   return  "Thèse d'établissement";    break;
		case 'OTHERREPORT': return  "Autre rapport, séminaire, workshop";   break;
		case 'REPACT':      return "Rapport d'activité";      break;
		case 'SYNTHESE':    return  "Notes de synthèse";      break;
		default :         return $code;                                 break;
	}		
}

// retourne le libellé du sous-type standard en fonction de son code
function getSousTypeDoc($code){
	switch($code){
		case 'ARTREV':		  	return "Article de synthèse";					break;
		case 'DATAPAPER':    	return "Data paper";    						break;
		case 'BOOKREVIEW':    	return "Compte-rendu de lecture";    			break;
		case 'DICTIONARY':    	return "Dictionnaire, encyclopédie";    		break;
		case 'SYNTOUV':    		return "Ouvrage de synthèse";    				break;
		case 'MANUAL':    		return "Manuel";    							break;
		case 'CRIT':    		return "Edition critique";    					break;
		case 'PREPRINT':    	return "Preprint";    							break;
		case 'WORKINGPAPER':    return "Working paper";    						break;
		case 'RESREPORT':    	return "Rapport de recherche";    				break;
		case 'TECHREPORT':    	return "Rapport technique";    					break;
		case 'FUNDREPORT':    	return "Rapport contrat / projet";    			break;
		case 'EXPERTREPORT':    return "Rapport d'expertise collective";    	break;
		case 'DMP':    			return "Plan de gestion de données";    		break;
		case 'PHOTOGRAPHY':    	return "Photographie";    						break;
		case 'DRAWING':    		return "Dessin";    							break;
		case 'ILLUSTRATION':    return "Illustration";    						break;
		case 'GRAVURE':    		return "Gravure";    							break;
		case 'GRAPHICS':    	return "Images de synthèse";    				break;
		case 'OUV':				return "Ouvrage"; 								break;
		default :         		return $code;                                   break;
	}		
}



function mb_ucwords($str) {
  $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
  return ($str);
}
function prenomCompInit($prenom) {
  $prenom = str_replace("  ", " ",$prenom);
  if (strpos(trim($prenom),"-") !== false) {//Le prénom comporte un tiret
    $postiret = mb_strpos(trim($prenom),'-', 0, 'UTF-8');
    if ($postiret != 1) {
      $prenomg = trim(mb_substr($prenom,0,($postiret-1),'UTF-8'));
    }else{
      $prenomg = trim(mb_substr($prenom,0,1,'UTF-8'));
    }
    $prenomd = trim(mb_substr($prenom,($postiret+1),strlen($prenom),'UTF-8'));
    $autg = mb_substr($prenomg,0,1,'UTF-8');
    $autd = mb_substr($prenomd,0,1,'UTF-8');
    $prenom = mb_ucwords($autg).".-".mb_ucwords($autd).".";
  }else{
    if (strpos(trim($prenom)," ") !== false) {//plusieurs prénoms
      $tabprenom = explode(" ", trim($prenom));
      $p = 0;
      $prenom = "";
      while (isset($tabprenom[$p])) {
        if ($p == 0) {
          $prenom .= mb_ucwords(mb_substr($tabprenom[$p], 0, 1, 'UTF-8')).".";
        }else{
          $prenom .= " ".mb_ucwords(mb_substr($tabprenom[$p], 0, 1, 'UTF-8')).".";
        }
        $p++;
      }
    }else{
      $prenom = mb_ucwords(mb_substr($prenom, 0, 1, 'UTF-8')).".";
    }
  }
  return $prenom;
}

function getAuteurs($tabAuteurs) {
	if (is_array($tabAuteurs))
		$ret = implode(", ",$tabAuteurs);
	else
		$ret = str_replace('"','',$tabAuteurs);
	return $ret;
}

function isPrenomCompose($nom){
    // on veut savoir si le prénom est composé de deux intiales, ex : J.F. Picard
	$tab = explode(' ',$nom); 
   	if ( substr_count($tab[0],'.') == 2  &&  strlen($tab[0])==4 )
		return true;
	else
		return false;
}
function getInitialCompose($nom){
	// si le nom est de la forme J.F. Bernard alors on retourne Bernard J.F.
	$tab = explode('. ',$nom); 
	return ($tab[0].'.');
}

/* obsolete 
function getAuteursDate($notice) {
	global $formeAut;
	if ($formeAut==0){ // affichage classique des noms d'auteur
		if (is_array($notice['authFullName_s'])) {
			$ret = implode(', ',$notice['authFullName_s']);
		} else {
			$ret = $notice['authFullName_s'];
		}
	}else{
		$i=0;
		foreach ($notice['authFirstName_s'] as $prenom){
			if (isPrenomCompose($notice['authFullName_s'][$i])) 
				$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
			else
				$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($prenom) .", ";
			$i++;
		}
		$ret = substr($ret,0,-2);
	}
	
	if (isset($notice['producedDate_s']))
		$ret .=	" (".$notice['producedDate_s']."). ";
	else
		$ret .=". ";
	return $ret;
}
*/

/*********************************************************/
/* Recherche de la position de l'auteur dans la liste  */
/* $value prend par exemple Emmanuelle Loizon_FacetSep_1116433_FacetSep_emmanuelle-loizon   */
/* recherche à partir de l'idHal saisi dans le formulaire  */
function placeAuteur($notice,$idHal){
	$i=1;
	foreach ($notice['authFullNamePersonIDIDHal_fs'] as $value){
		if ( strpos($value,$idHal) !== false )
			$place = $i;
		$i++;
	}
	return $place;
}

function getNomPrenomByPosition($notice,$position){
	global $formeAut;
	$position = $position-1; /* rectification pour indice tableau */
	
	if ($formeAut==1){ 		
		if (isPrenomCompose($notice['authFullName_s'][$position])) 
			$ret = $notice['authLastName_s'][$position] ." ". getInitialCompose($notice['authFullName_s'][$position]);
		else
			$ret = $notice['authLastName_s'][$position] ." ". prenomCompInit($notice['authFirstName_s'][$position]);
		return $ret;
	} else {  // affichage classique des noms d'auteur	
		return $notice['authFullName_s'][$position];
	}
}



/***
1         L’auteur qui fait l’export est présent dans les 10 premiers auteurs > mettre et al. après le 10e auteur

2         L’auteur n’est pas présent dans les 10 premiers auteurs et n’est pas le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué, mettre et al.

3         L’auteur n’est pas présent dans les 10 premiers auteurs et il est le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué en dernier sans et al.

*/
function getAuteursDate($notice) {
	global $formeAut;
	global $idhal;
	
	$positionAuteur = placeAuteur($notice,$idhal);
	$i=0;
	$limit = 10;
	$nbAuteurs = count($notice['authFirstName_s']);
		
	
	switch ($formeAut) {
		
		case 0: // affichage classique des noms d'auteur		
				
			/* Cas où il y a moins de 10 auteurs  : on les affiche tous  */	
			if ( $nbAuteurs <= $limit ) {
				foreach ($notice['authFullName_s'] as $fullname)
					$ret .= $fullname .", ";	
				$ret = substr($ret,0,-2);
				
				
			/* A partir de onze auteurs */	
			} else {
			/****************************/ 			
				
				/* l'auteur est 11/11 : on les prend tous */
				if ($positionAuteur == 11 && $nbAuteurs == 11) {
					foreach ($notice['authFullName_s'] as $fullname)
						$ret .= $fullname .", ";	
					$ret = substr($ret,0,-2);
				
				} else {
				
				
					/* L’auteur qui fait l’export est présent dans les 10 premiers auteurs > mettre et al. après le 10e auteur  */
					if ($positionAuteur <= $limit) {
						for ($i=0; $i < $limit; $i++){
							$fullname = $notice['authFullName_s'][$i];
							$ret .= $fullname .", ";						
						}
						$ret = substr($ret,0,-2);
						$ret .= " et al.";
					
					} else {			
							
						/* L’auteur n’est pas présent dans les 10 premiers auteurs et n’est pas le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué, mettre et al. */
						if ($positionAuteur > $limit && $positionAuteur < $nbAuteurs) {
							for ($i=0; $i < $limit; $i++){
								$fullname = $notice['authFullName_s'][$i];
								$ret .= $fullname .", ";						
							}
							$ret = substr($ret,0,-2);
							$ret .= " ... ";
							$nomAuteurForm = getNomPrenomByPosition($notice,$positionAuteur);
							$ret .= $nomAuteurForm;
							$ret .= " et al.";
						
						
						} else {
						
							/* L’auteur n’est pas présent dans les 10 premiers auteurs et il est le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué en dernier sans et al. */
							if ($positionAuteur > $limit && $positionAuteur == $nbAuteurs) {
								for ($i=0; $i < $limit; $i++){
									$fullname = $notice['authFullName_s'][$i];
									$ret .= $fullname .", ";						
								}
								$ret = substr($ret,0,-2);
								$ret .= " ... ";
								$nomAuteurForm = getNomPrenomByPosition($notice,$positionAuteur);
								$ret .= $nomAuteurForm;
							}
						}					
					}
				}
			}		
		break;
	
		case 1: /* CAS OU LA FORME AUTEUR EST DE TYPE Nom P. */
			
			/* Cas où il y a moins de 10 auteurs  : on les affiche tous  */
			if ( $nbAuteurs <= $limit ) {
				foreach ($notice['authFirstName_s'] as $prenom){
					if (isPrenomCompose($notice['authFullName_s'][$i])) 
						$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
					else
						$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($prenom) .", ";
					$i++;
				}
				$ret = substr($ret,0,-2);
				
			/* A partir de onze auteurs */	
			} else {
			/****************************/
				
				/* l'auteur est 11/11 : on les prend tous */
				if ($positionAuteur == 11 && $nbAuteurs == 11) {
					for ($i=0; $i < 11; $i++){
						if (isPrenomCompose($notice['authFullName_s'][$i])) 
							$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
						else
							$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($notice['authFirstName_s'][$i]) .", ";				
					}
					$ret = substr($ret,0,-2);
				
				} else {
								
					
					/* L’auteur qui fait l’export est présent dans les 10 premiers auteurs > mettre et al. après le 10e auteur  */
					if ($positionAuteur <= $limit ) {
						for ($i=0; $i < $limit; $i++){
							if (isPrenomCompose($notice['authFullName_s'][$i])) 
								$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
							else
								$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($notice['authFirstName_s'][$i]) .", ";				
						}
						$ret = substr($ret,0,-2);
						$ret .= " et al.";
					
					} else {								
						
						/* L’auteur n’est pas présent dans les 10 premiers auteurs et n’est pas le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué, mettre et al. */
						if ($positionAuteur > $limit && $positionAuteur < $nbAuteurs) {
							for ($i=0; $i < $limit; $i++){
								if (isPrenomCompose($notice['authFullName_s'][$i])) 
									$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
								else
									$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($notice['authFirstName_s'][$i]) .", ";				
							}
							$ret = substr($ret,0,-2);
							$ret .= " ... ";
							$nomAuteurForm = getNomPrenomByPosition($notice,$positionAuteur);
							$ret .= $nomAuteurForm;
							$ret .= " et al.";
							
							
						} else {
						
							/* L’auteur n’est pas présent dans les 10 premiers auteurs et il est le dernier auteur de la liste > points de suspension après le 10e auteur, faire apparaître le nom de l’auteur évalué en dernier sans et al. */
							if ($positionAuteur > $limit && $positionAuteur == $nbAuteurs) {
								for ($i=0; $i < $limit; $i++){
									if (isPrenomCompose($notice['authFullName_s'][$i])) 
										$ret .= $notice['authLastName_s'][$i] ." ". getInitialCompose($notice['authFullName_s'][$i]) .", ";
									else
										$ret .= $notice['authLastName_s'][$i] ." ". prenomCompInit($notice['authFirstName_s'][$i]) .", ";				
								}
								$ret = substr($ret,0,-2);
								$ret .= " ... ";
								$nomAuteurForm = getNomPrenomByPosition($notice,$positionAuteur);
								$ret .= $nomAuteurForm;
							}
						}									
					}
				}
			}
		break;
	}
	
	
	//$ret .= "  **Pos: ".$positionAuteur." / ".$nbAuteurs."** ";
	
	if (isset($notice['producedDate_s']))
		$ret .=	" (".$notice['producedDate_s']."). ";
	else
		$ret .=". ";
	return $ret;
}





function cleanType($type){
	$type = str_replace(" - ]","]",$type);
	$type = str_replace("[ - ","[",$type);
	return $type;
}


/////////////////////////////////////////////////////////////////////
// fonctions de nomenclature
////////////////////////////////////////////////////////////////
function ecritLienOA($notice,&$sectionRtf){
	global $fontOrange;
	if ( isset($notice['openAccess_bool'])) {
		if ($notice['openAccess_bool'] == true){ 
			if ( isset($notice['linkExtUrl_s']))
				$lien = $notice['linkExtUrl_s'];
			if ( isset($notice['fileMain_s']))
				$lien = $notice['fileMain_s'];
			if ( isset($lien) ) {
				$sectionRtf->writeText(', ');
				$sectionRtf->writeHyperLink($lien,"OA",$fontOrange);
			}
		}
	}
}

// permet de savoir si une notice est OA ou pas
function isOA($notice){
	if ( isset($notice['openAccess_bool'])) {
		if ($notice['openAccess_bool'] == true){ 
			if ( isset($notice['linkExtUrl_s']) || isset($notice['fileMain_s']) )
				return true;
			else 
				return false;
		} else {
			return false;
		}
	} else { return false; }
}

function ecritOA($notice,&$sectionRtf,$mention=false){
	global $fontOrange;
	global $fontViolet;
	
	if ( isOA($notice)) 		
		ecritLienOA($notice,$sectionRtf);		
	else {
		if ($mention){
			$sectionRtf->writeText(', ');
			$lien = "https://export.hal.inrae.fr/css/evaluation_inrae.html";
			$sectionRtf->writeHyperLink($lien,"ce document n'est pas en libre accès !",$fontViolet);
		}
	}
}

function ecritArticle($notice,&$sectionRtf,$sousType=false,$doctype=false,$mentionLibreAcces=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ($doctype)
		$sectionRtf->writeText("[".getTypeDoc($notice['docType_s'])."] ");
	if ($sousType){
		$ssType = getSousTypeDoc($notice['docSubType_s']);
		if ($ssType!="")
			$sectionRtf->writeText("[".$ssType."] ");
	}
	
	
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre);
	}
	if (isset($notice['journalTitle_s'])) {
		$journal= htmlspecialchars($notice['journalTitle_s']).", ";
		$sectionRtf->writeText($journal,$fontItalic);
	}
	if (isset($notice['volume_s'])) 				$sectionRtf->writeText($notice['volume_s']);
	else $double=true;
	// attention double espace si pas de volume
	
	// mettre une virgule apres le volume si pas d'issue_s
	if (isset($notice['issue_s']) && $double) 	
		$sectionRtf->writeText("(".$notice['issue_s'][0].'), ');
	if (isset($notice['issue_s']) && !$double) 	
		$sectionRtf->writeText(" (".$notice['issue_s'][0].'), ');
	
	if (!isset($notice['issue_s'])) $sectionRtf->writeText(", ");
	
	if (isset($notice['page_s'])) 					$sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritOA($notice,$sectionRtf,$mentionLibreAcces);  // permet de soit écrire le lien OA soit Ce document n’est pas en libre accès !
	
	$sectionRtf->writeText('<br />');
}

function ecritIssue($notice,&$sectionRtf,$sousType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ($sousType)
		$sectionRtf->writeText("[N° spécial de revue] ");
		
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre);
	}
	if (isset($notice['journalTitle_s'])) {
		$journal= htmlspecialchars($notice['journalTitle_s']).", ";
		$sectionRtf->writeText($journal,$fontItalic);
	}
	if (isset($notice['volume_s'])) 				$sectionRtf->writeText($notice['volume_s']);
	else $double=true;
	// attention double espace si pas de volume
	
	// mettre une virgule apres le volume si pas d'issue_s
	if (isset($notice['issue_s']) && $double) 	
		$sectionRtf->writeText("(".$notice['issue_s'][0].'), ');
	if (isset($notice['issue_s']) && !$double) 	
		$sectionRtf->writeText(" (".$notice['issue_s'][0].'), ');
	
	if (!isset($notice['issue_s'])) $sectionRtf->writeText(", ");
	
	if (isset($notice['page_s'])) 					$sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);  // permet de soit écrire le lien OA soit Ce document n’est pas en libre accès !
	
	$sectionRtf->writeText('<br />');
}

function ecritPreprintWp($notice,&$sectionRtf,$sousType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($sousType){
		if ( $notice['docSubType_s'] == "WORKINGPAPER" ){
			$sectionRtf->writeText("[Working Paper] ");
		}
	}
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['serie_s'])) {
		$serie= htmlspecialchars($notice['serie_s'][0]).", ";
		$sectionRtf->writeText($serie,$fontItalic);
	}
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	if (isset($notice['arxivId_s'])) {
		$sectionRtf->writeText("n° arxiv: ",$fontBleu);
		$sectionRtf->writeText($notice['arxivId_s'].", ");
	}
	if (isset($notice['biorxivId_s'])){
		$sectionRtf->writeText("n° biorxiv: ",$fontBleu);
		$sectionRtf->writeText($notice['biorxivId_s'][0].", ");
	}	
	//if (isset($notice['page_s'])) 					$sectionRtf->writeText($notice['page_s'].', ');
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	
	$sectionRtf->writeText('<br />');
}

function ecritOuv($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype){
		$ssType = getSousTypeDoc($notice['docSubType_s']);
		if ( $ssType != "" )
			$sectionRtf->writeText("[".$ssType."] ");
		else $sectionRtf->writeText("[Ouvrage] ");
	}
		
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['publisher_s'][0])) $sectionRtf->writeText(htmlspecialchars($notice['publisher_s'][0]).', ');
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

// Citation TRAD ou BLOG
function ecritTradBlog($notice,&$sectionRtf,$sousType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($sousType){
		if ( $notice['docType_s'] == "TRAD" )
			$sectionRtf->writeText("[Traduction] ");
		 else
			$sectionRtf->writeText("[Article de blog scientifique] ");
	}
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['description_s']))  $sectionRtf->writeText($notice['description_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritProceeding($notice,&$sectionRtf,$docType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ($docType)
		$sectionRtf->writeText("[Proceedings] ");
	
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['publisher_s'][0])) $sectionRtf->writeText(htmlspecialchars($notice['publisher_s'][0]).', ');
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}



function ecritChapitreEncyclo($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ($doctype){
		if ($notice['docType_s']=='NOTICE') $sectionRtf->writeText("[Notice d'encyclopédie ou de dictionnaire] ");
		if ($notice['docType_s']=='COUV')   $sectionRtf->writeText("[Chapitre d'ouvrage] ");
	}
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars($notice['title_s'][0]).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre);
	}
	if (isset($notice['bookTitle_s'])) {
		$sectionRtf->writeText('In : ',$fontBleu);
		$sectionRtf->writeText(htmlspecialchars($notice['bookTitle_s']).'. ',$fontItalic);
	}
	if (isset($notice['publisher_s'][0])) $sectionRtf->writeText(htmlspecialchars($notice['publisher_s'][0]).', ');
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritHdr($notice,&$sectionRtf){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['authorityInstitution_s'])) $sectionRtf->writeText("(".$notice['authorityInstitution_s'][0]."). ");
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritComm($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype)
		$sectionRtf->writeText("[Communication] ");
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['conferenceTitle_s'])) {
		$sectionRtf->writeText("Presented at : ",$fontBleu);
		$sectionRtf->writeText(htmlspecialchars($notice['conferenceTitle_s']).", ");
	}
	if (isset($notice['city_s'])) 				$sectionRtf->writeText($notice['city_s'].", ");
	if (isset($notice['country_s'])) 			$sectionRtf->writeText(code_to_country($notice['country_s'])." ");
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceStartDate_s'])){
		$conf = "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."), ";
		$conf = str_replace(" - )",")",$conf); 
		$sectionRtf->writeText($conf);
	}
	if ( $notice['invitedCommunication_s'] == "1" )
			$sectionRtf->writeText("conférence invitée. ");		
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritPoster($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ( $doctype )
		$sectionRtf->writeText("[Poster] ");
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['conferenceTitle_s'])) {
		$sectionRtf->writeText("Presented at : ",$fontBleu);
		$sectionRtf->writeText(htmlspecialchars($notice['conferenceTitle_s']).", ");
	}
	if (isset($notice['city_s'])) 				$sectionRtf->writeText($notice['city_s'].", ");
	if (isset($notice['country_s'])) 			$sectionRtf->writeText(code_to_country($notice['country_s'])." ");
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceStartDate_s'])){
		$conf = "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."), ";
		$conf = str_replace(" - )",")",$conf); 
		$sectionRtf->writeText($conf);
	}
	if ( $notice['invitedCommunication_s'] == "1" )
			$sectionRtf->writeText("conférence invitée. ");		
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritLogiciel($notice,&$sectionRtf,$docType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ( $docType )
		$sectionRtf->writeText("[Logiciel] ");
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}



function ecritRapport($notice,&$sectionRtf,$sousType=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	
	if ($sousType){
		$ssType = getSousTypeDoc($notice['docSubType_s']);
		if ($ssType!="")
			$sectionRtf->writeText("[".$ssType."] ");
	}
		
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['authorityInstitution_s']))  	$sectionRtf->writeText("(".htmlspecialchars($notice['authorityInstitution_s'][0])."), ");
	if (isset($notice['page_s'])) 					$sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['number_s'])) 				$sectionRtf->writeText(htmlspecialchars($notice['number_s'][0]).', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritNorme($notice,&$sectionRtf){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritVideoSon($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype) {
		$type = "[". getTypeDoc($notice['docType_s']) ."] ";
		$sectionRtf->writeText($type);
	}
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritImage($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype)
		$sectionRtf->writeText("[Image] "); 
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['country_s'])) $sectionRtf->writeText(code_to_country($notice['country_s']).', ');
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritBrevet($notice,&$sectionRtf){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	$sectionRtf->writeText("[".getTypeDoc($notice['docType_s'])."] ");
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['number_s'])) {
		$sectionRtf->writeText("(");
		$sectionRtf->writeText("Brevet n°: ",$fontBleu);
		$sectionRtf->writeText(htmlspecialchars($notice['number_s'][0]) . "). ");
	}
	if (isset($notice['country_s'])) $sectionRtf->writeText(code_to_country($notice['country_s']));
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritCours($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype)
		$sectionRtf->writeText("[Cours] "); 
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ",$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['lectureName_s']) || isset($notice['lectureType_s']) || isset($notice['authorityInstitution_s'])){
		$cours = "(". $notice['lectureName_s'] .", ". getLectureType($notice['lectureType_s']) .", ". $notice['authorityInstitution_s'][0] .") ";
		$cours = str_replace('(, ','(',$cours);
		$cours = str_replace(', )',')',$cours);
		$sectionRtf->writeText($cours);
	}
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritCarte($notice,&$sectionRtf,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');
	if ($doctype)
		$sectionRtf->writeText("[Carte] ");
	$sectionRtf->writeText(getAuteursDate($notice));
	$titre = htmlspecialchars(strip_tags($notice['title_s'][0])).". ";
	$sectionRtf->writeText($titre,$fontItalic);
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['country_s'])) $sectionRtf->writeText(code_to_country($notice['country_s']).' ');
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function ecritOther($notice,&$sectionRtf,$sousType=false,$doctype=false){
	global $fontItalic,$fontBleu;
	$sectionRtf->writeText('<br />');

	if ($doctype) 
		$sectionRtf->writeText("[".getTypeDoc($notice['docType_s'])."] ");
	
	if ($sousType){
		$ssType = getSousTypeDoc($notice['docSubType_s']);
		if ($ssType!="")
			$sectionRtf->writeText("[".$ssType."] ");
	}
	$sectionRtf->writeText(getAuteursDate($notice));
	$sectionRtf->writeText(htmlspecialchars(strip_tags($notice['title_s'][0])).". ");
	if (isset($notice['subTitle_s'])) {
		$ssTitre= htmlspecialchars($notice['subTitle_s'][0]).". ";
		$sectionRtf->writeText($ssTitre,$fontItalic);
	}
	if (isset($notice['bookTitle_s'])) {
		$sectionRtf->writeText('In : ',$fontBleu);
		$sectionRtf->writeText(htmlspecialchars($notice['bookTitle_s']).'. ');
	}
	if (isset($notice['journalTitle_s'])) {
		$journal= htmlspecialchars($notice['journalTitle_s']).", ";
		$sectionRtf->writeText($journal);
	}
	if (isset($notice['volume_s'])) $sectionRtf->writeText($notice['volume_s'].' ');
	if (isset($notice['issue_s'])) $sectionRtf->writeText("(".$notice['issue_s'][0].'), ');
	if (isset($notice['page_s'])) $sectionRtf->writeText($notice['page_s'].', ');
	if (isset($notice['publisher_s'][0])) $sectionRtf->writeText(htmlspecialchars($notice['publisher_s'][0]).', ');
	if (isset($notice['doiId_s'])) {
		$doi = changeDoi($notice['doiId_s']);
		$sectionRtf->writeHyperLink($doi,$doi,$fontBleu);
		$sectionRtf->writeText(', ');
	}
	$lien = changeUri($notice);
	$sectionRtf->writeHyperLink($lien,$lien,$fontBleu);
	
	ecritLienOA($notice,$sectionRtf);
	$sectionRtf->writeText('<br />');
}

function getNbNonTraitee($aCompter, $traitees){
	$nbNoticeNonVentile = 0;
	foreach ($aCompter as $notice) {
		if (!in_array($notice['halId_s'], $traitees)){
			$nbNoticeNonVentile++;
		}
	}
	return $nbNoticeNonVentile;
}

?>