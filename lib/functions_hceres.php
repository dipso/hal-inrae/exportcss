<?php

/* Fonctions pour l'export HCERES
calcul des citations pour chaque onglet du fichier excel
*/

// fichier comportant la fonction pays => language
@include_once("fct_lang.php");

//Suppresion des accents
function wd_remove_accents($str, $charset='utf-8') {
	$str = htmlentities($str, ENT_NOQUOTES, $charset);
	$str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
	$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
	return preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
}

function nettoy1($quoiAvt) {
  $quoiApr = str_replace(array(". : ",",, ",", , ","..","?.","?,","<br>.","--"," p. p.",", .",",  ","(dir. ."," - .., ",", , ", ", . "), array(" : ",", ",", ",".","?","?","<br>","-"," p.",",",", ","(dir.). ",", ",", ",". "), $quoiAvt);
  return($quoiApr);
}



function prenomCompEntier($prenom) {
  $prenom = trim($prenom);
  if (strpos($prenom,"-") !== false) {//Le prénom comporte un tiret
    $postiret = strpos($prenom,"-");
    $autg = substr($prenom,0,$postiret);
    $autd = substr($prenom,($postiret+1),strlen($prenom));
    $prenom = mb_ucwords($autg)."-".mb_ucwords($autd);
  }else{
    $prenom = mb_ucwords($prenom);
  }
  return $prenom;
}

function nomCompEntier($nom) {
  $nom = trim(mb_strtolower($nom,'UTF-8'));
  if (strpos($nom,"-") !== false) {//Le nom comporte un tiret
    $postiret = strpos($nom,"-");
    $autg = substr($nom,0,$postiret);
    $autd = substr($nom,($postiret+1),strlen($nom));
    $nom = mb_ucwords($autg)."-".mb_ucwords($autd);
  }else{
    $nom = mb_ucwords($nom);
  }
  return $nom;
}

function getAuteursHceres($notice) {
	$i=0;
	foreach ($notice['authFirstName_s'] as $prenom){
		if (isPrenomCompose($notice['authFullName_s'][$i])) 
			$ret .= strtoupper(wd_remove_accents($notice['authLastName_s'][$i])) ." ". getInitialCompose(wd_remove_accents($notice['authFullName_s'][$i])) .", ";
		else
			$ret .= strtoupper(wd_remove_accents($notice['authLastName_s'][$i])) ." ". wd_remove_accents(prenomCompInit($prenom)) .", ";
		$i++;
	}
	$ret = substr($ret,0,-2);
	return $ret;
}

// $nomComplet = authFullName_s
// $nom        = authLastName_s
// $prenom     = authFirstName_s
function getNomAuteur($nom,$prenom,$nomComplet){
	if (isPrenomCompose($nomComplet)) 
		return strtoupper(wd_remove_accents($nom)) ." ". getInitialCompose(wd_remove_accents($nomComplet));
	else
		return strtoupper(wd_remove_accents($nom)) ." ". wd_remove_accents(prenomCompInit($prenom));
}

// $labo = $equipes = 1002209~574738~74637~100299 liste de docid structure à partir du champ Informations laboratoires
// $soulignement : booléen pour activer le soulignement des auteurs
// dans authIdHasPrimaryStructure_fs on a comme valeur par ex : 50285-744358_FacetSep_Hélène Jammes_JoinSep_1003282_FacetSep_Biologie de la Reproduction....
function getAuteursSoulign($notice,$soulignement,$labo) {
	if ($soulignement == 0)
		return getAuteursHceres($notice);
	else {
		
		$objRichText = new PHPExcel_RichText();
		$nbAuteur = count($notice['authFirstName_s']);
		$TabATester = explode('~',$labo);
		$i=0; 
				
		foreach ($notice['authFullName_s'] as $auteur) {    // authFullName_s contient par ex "Amanda de Mestre"
			$nomAuteur = getNomAuteur($notice['authLastName_s'][$i],$notice['authFirstName_s'][$i],$notice['authFullName_s'][$i]);
			
			if (isSoulignable($notice,$auteur,$TabATester)) {
			// si l'auteur a une affiliation qui matchent avec la liste fournie
				$objUnderlined = $objRichText->createTextRun($nomAuteur);
				$objUnderlined->getFont()->setUnderline(true);
			} else {
				$objRichText->createText($nomAuteur);
			}
			if ($i < $nbAuteur-1)
				$objRichText->createText(", ");
			$i++;
		}
		
		//$objRichText->createText(" -  tab à tester: ".implode('_',$TabATester)." labo: ".$labo);
		
		return $objRichText;	
	}
}

function isSoulignable($notice,$auteur,$TabLabo){
	$ret = false;
	foreach ($notice['authIdHasPrimaryStructure_fs'] as $auteurPS){	// pour chaque auteur rattaché à une structure primaire	
		//si l'auteur qu'on cherhce se trouve dans cette entrée alors on regarde si sa structure matche
		if (strpos($auteurPS, $auteur) !== false){
			// l'auteur est trouvé dans la chaine donc on peut chercher s'il y a une affiliation à souligner
			$tabAuteurAff = explode("_JoinSep_",$auteurPS);
			$affiliation = $tabAuteurAff[1];  // contient par ex : 1050246_FacetSep_Royal Veterinary
			$tabAff  = explode("_FacetSep_",$affiliation);
			$codeAff = $tabAff[0];
			if (in_array($codeAff,$TabLabo))
				$ret = true;
		}
	}
	return $ret;
}

function getLangueHceres($notice){
	if (isset($notice['language_s'])) {
		return code_to_language($notice['language_s'][0]);
	} else {
		if (isset($notice['country_s']))
			return code_to_country($notice['country_s']);
		else
			return "";
	}
}

function getAnnee($notice){	
	if (isset($notice['producedDate_s'])) {
		$pos = strpos($notice['producedDate_s'], '-');
		if ( $pos === false ) { // non trouvé
			if ( strlen($notice['producedDate_s']) == 4 )
				return $notice['producedDate_s'];
			else
				return "autre1";
		} else {  // la date comporte au moins un tiret
			$tabDate = explode("-",$notice['producedDate_s']);
			if ( strlen($tabDate[0]) == 4)
				return $tabDate[0];
			else
				return "autre";
		}
	} else {
		return "undefined";
	}
}


// Retourne la liste des affiliations qu'il faut comparer à celles des auteurs
// cette liste est obtenue à partir de ce que l'utilisateur a saisi dans le champ EquipeLabo
function getAffiliations($equipe){	
	$tabEquipeLabo = explode("~", $equipe); //%7E <=> ~
	$tabStruct = array();
	
	foreach($tabEquipeLabo as $equipeLabo) {
		if (is_numeric($equipeLabo)) 
			$req = "https"."://api.archives-ouvertes.fr/ref/structure/?q=docid:".$equipeLabo."%20AND%20country_s:%22fr%22&fl=docid";
		else
			$req = "https"."://api.archives-ouvertes.fr/ref/structure/?q=(name_t:".$equipeLabo."%20OR%20acronym_t:".$equipeLabo.")%20AND%20valid_s:(VALID%20OR%20OLD)%20AND%20country_s:%22fr%22&fl=docid";
			// Exemple avec ECOBIO retourne 1 valeur : docid=928 https://api.archives-ouvertes.fr/ref/structure/?q=(name_t:ECOBIO%20OR%20acronym_t:ECOBIO)%20AND%20valid_s:(VALID%20OR%20OLD)%20AND%20country_s:%22fr%22&fl=docid
		
		$resultat = file_get_contents($req);
		$res = json_decode($resultat);
		foreach($res->response->docs as $valeur)
			$tabStruct[] = $valeur->docid;
	}
	return implode('~',array_unique($tabStruct));  
}

function getIndexAuteur($notice,$docid){
	$i=0;
	$index = 0;
	foreach ($notice['authIdFullName_fs'] as $auteur){
		$pos = strpos($auteur,$docid); // on cherche $docid (12596751) dans une chaine du type  12596751_FacetSep_N. Morellet
		if ($pos !== false)
			$index = $i;
		$i++;
	}
	return $index;  // on retourne l'index de l'auteur dans ce tableau qui servira à retrouver son role dans authQuality_s
}

// 
function getIndexAuteurByName($notice,$nomAuteur){
	$i=0;
	$index = 0;
	foreach ($notice['authIdFullName_fs'] as $auteur){
		// on cherche le prenomù et le nom de l'auteur dans une chaine du type 743774_FacetSep_Aurélie Nicolas
		if (strpos($auteur,$nomAuteur) !== false) 
			$index = $i;
		$i++;
	}
	return $index;  // on retourne l'index de l'auteur dans ce tableau qui servira à retrouver son role dans authQuality_s
}

// Parametres : une notice et la liste des structures à tester $equipes = getAffiliations(...)
// si Identifiant structure (($equipeLabo) saisi est "1002209~AGIR" alors $equipes = 1002209~574738~74637~100299~560019~1002209~552012~225605~531394
// Ex de valeur pour authIdHasPrimaryStructure_fs ["1643348_FacetSep_Patrick Baldet_JoinSep_566980_FacetSep_Écosystèmes forestiers","11996480_FacetSep_Fabienne Colas_JoinSep_1021679_FacetSep_Direction de la recherche forestière"]
// dans chaque case du tableau: un auteur, pour chaque auteur on a DOCID auteur FacetSep Prénom Nom _JoinSep_ DOCID Structure FacetSep Nom structure
// Ex de valeur pour authQuality_s ["aut","crp"]
// Ex authLastName_s":["Baldet","Colas"]
function getPremierDernier($notice,$lstEquipe){
	$retour = 'N';
	
	if ($lstEquipe=="")  // si rien a comparé
		return $retour;
	
	$i=0;
	$nbAuteur = count($notice['authQuality_s']);
	$TabATester = explode('~',$lstEquipe);
	
	foreach ($notice['authIdHasPrimaryStructure_fs'] as $auteur){	// pour chaque auteur affilié
		$tabAuteur = explode("_JoinSep_",$auteur); // on sépare l'auteur de sa structure $tabAuteur[0] contient la partie auteur ex: 1643348_FacetSep_Patrick Baldet
		// $tabAuteur[1] contient la partie structure ex: 566980_FacetSep_Écosystèmes forestiers
		$tabAuteurStruct = explode("_FacetSep_",$tabAuteur[1]); 
        $tabAuteurDocid  = explode("_FacetSep_",$tabAuteur[0]);
		$idStructAuteur = $tabAuteurStruct[0];   // contient le DOCID de la structure de l'auteur
		$docidAuteur = $tabAuteurDocid[0];   // contient le DOCID de l'auteur affilié 
		$nomAuteur = $tabAuteurDocid[1];  // contient le nom de l'auteur ex : Eric Guédon
		
		//$indexAuteur = getIndexAuteur($notice,$docidAuteur);
		$indexAuteur = getIndexAuteurByName($notice,$nomAuteur);
		// il y a parfois plus d'affiliations que d'auteur donc il faut retourver le numéro d'auteur dans la liste pour retrouver son role
		
		if ($indexAuteur == 0){ // on teste le premier auteur et son appartenance à la liste $lstEquipe
			if (in_array($idStructAuteur,$TabATester)){
				$retour='O';
				$rep.= $idStructAuteur." 1er auteur trouvé dans ".print_r($TabATester,true);
			}
		}
		
		if ($indexAuteur == $nbAuteur-1){  // on teste le dernier auteur et son appartenance à la liste $lstEquipe
			if (in_array($idStructAuteur,$TabATester)){
				$retour='O';
				$rep.= $idStructAuteur." dernier auteur trouvé dans ".print_r($TabATester,true);
			}
		}
		
		
		if ($notice['authQuality_s'][$indexAuteur] == "crp") {  // on teste l'auteur correspondant
			if (in_array($idStructAuteur,$TabATester)){
				$retour='O';
				$rep.= $idStructAuteur." auteur correspondant trouvé dans ".print_r($TabATester,true);
			}
		}
		
		if ($notice['authQuality_s'][$indexAuteur] == "co_first_author") {  // on teste le premier co-auteur
			if (in_array($idStructAuteur,$TabATester)){
				$retour='O';
				$rep.= $idStructAuteur." 1er co-auteur trouvé dans ".print_r($TabATester,true);
			}
		}
		
		if ($notice['authQuality_s'][$indexAuteur] == "co_last_author") {  // on teste le dernier co-auteur
			if (in_array($idStructAuteur,$TabATester)){
				$retour='O';
				$rep.= $idStructAuteur." dernier co-auteur trouvé dans ".print_r($TabATester,true);
			}
		}		
			
		$i++;
	}
	
	$rep .= " car ".print_r($notice['authIdHasPrimaryStructure_fs'],true). " et ".print_r($notice['authQuality_s'],true)."  a comparer à ".$lstEquipe; 
   /* if($notice['halId_s']=="hal-03713030"){
		$indexAuteur = getIndexAuteur($notice,"1146249");
		$rep = " index  Chinju Johnson : ".$indexAuteur." ".print_r($notice['authIdFullName_fs'],true);
	}*/
	
	return $retour; //.' --DEBUG: '.$rep;	
	
}

// Pour OA, c’est un test avec le champ ‘submitType_s’ afin de savoir s’il est ‘file’
// ou du champ ‘linkExtId_s’ afin de savoir s’il est ‘openaccess’, ‘arxiv’ ou ‘pubmedcentral’.
function getOpenAcess($notice){
	$oa = 'N';
	if ( $notice['submitType_s'] == "file" || $notice['linkExtId_s'] == "openaccess" || $notice['linkExtId_s'] == "arxiv" || $notice['linkExtId_s'] == "pubmedcentral" )
		$oa = 'O';
	
	return $oa;
}

function getVulgarisation($notice){
	if ($notice['popularLevel_s'] != "" && $notice['inra_publicVise_local_s'][0] != ""){
		if ($notice['popularLevel_s']=="1" || $notice['inra_publicVise_local_s'][0] == "GP")
			return "O";
		else
			return "N";
	} else {
		return "";
	} 
}

function getComite($notice){
	if ($notice['peerReviewing_s'] != ""){
		if ($notice['peerReviewing_s'] == "1")
			return "O";
		else
			return "N";
	} else {
		return "";
	}
}

function getConferenceInvitee($notice){
	if ($notice['invitedCommunication_s'] != ""){
		if ($notice['invitedCommunication_s'] == "1")
			return "O";
		else
			return "N";
	} else {
		return "";
	}
}

function getRapportExpertise($notice){
	if ($notice['inra_reportType_local_s'][0] != ""){
		if ($notice['inra_reportType_local_s'][0] == "RE")
			return "O";
		else
			return "N";
	} else {
		return "";
	}
}

function getTypePrepubli($notice){
	if (isset($notice['inra_otherType_Undef_local_s'][0])){
		if ($notice['inra_otherType_Undef_local_s'][0]=="PP")
			return "Preprint";
		if ($notice['inra_otherType_Undef_local_s'][0]=="WP")
			return "Working paper";
	} else {
		return "";
	}
}

function ssTypeOtherStd($code){
	switch($code){
		case "0" : return  ""; 			                                            break;
		case "1" : return  "Article de Blog"; 			                            break;
		case "2" : return  "Compte-rendu d'ouvrage ou Note de Lecture"; 			break;
		case "3" : return  "Notice d'encyclopédie ou de dictionnaire"; 	            break;
		case "4" : return  "Traduction"; 			                                break;
	}
}

function getAudience($notice){
	if (isset($notice['audience_s'])){
		switch($notice['audience_s']){
			case "1" : return  "Non spécifié"; 			break;
			case "2" : return  "Internationale"; 		break;
			case "3" : return  "Nationale"; 	        break;
			default  : return  "";   					break;
		}
	}
}

function getReportType($notice){
	if (isset($notice['reportType_s'])){
		switch($notice['reportType_s']){
			case "0" : return  "Autre"; 			            break;
			case "1" : return  "Travaux universitaires"; 		break;
			case "2" : return  "Contrat"; 	                    break;
			case "3" : return  "Rapport interne"; 			    break;
			case "4" : return  "Rapport technique"; 		    break;
			case "5" : return  "Rapport de stage"; 	            break;
			case "6" : return  "Rapport de recherche"; 	        break;
			
		}
	}
}

/*************************** CITATION  **************************************/
/****************************************************************************/

function getCitationArticle($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) {
		$ssTitre= $notice['subTitle_s'][0].". ";
		$ret .= $ssTitre;
	}
	if (isset($notice['journalTitle_s'])) {
		$journal= $notice['journalTitle_s'].", ";
		$ret .= $journal;
	}
	if (isset($notice['volume_s']))  
		$ret .= $notice['volume_s'];
	else 
		$double=true; 
	// attention double espace si pas de volume
	
	// mettre une virgule apres le volume si pas d'issue_s
	if (isset($notice['issue_s']) && $double) 	
		$ret .= "(".$notice['issue_s'][0]."), ";
	if (isset($notice['issue_s']) && !$double) 	
		$ret .= " (".$notice['issue_s'][0]."), ";
	
	if (!isset($notice['issue_s']))
		$ret .= ", ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationChapitreOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationDirectionOuvrage($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
	$ret .= "(".$notice['producedDate_s']."). ";
	
	if (isset($notice['title_s'])) 
		$ret .=  $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['conferenceTitle_s'])) 
		$ret .=  $notice['conferenceTitle_s'].", ";
	
	if (isset($notice['city_s'])) 
		$ret .=  $notice['city_s'].", ";
	
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s'])." ";
	
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceEndDate_s'])) 
		$ret .=  "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."). ";
	
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationCommunication($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
	$ret .= "(".$notice['producedDate_s']."). ";
	
	if (isset($notice['title_s'])) 
		$ret .=  $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['conferenceTitle_s'])) 
		$ret .=  "Presented at ".$notice['conferenceTitle_s'].", ";
	
	if (isset($notice['city_s'])) 
		$ret .=  $notice['city_s'].", ";
	
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s'])." ";
	
	if (isset($notice['conferenceStartDate_s']) || isset($notice['conferenceEndDate_s'])) 
		$ret .=  "(".$notice['conferenceStartDate_s']." - ".$notice['conferenceEndDate_s']."), ";
	
	if ($notice['invitedCommunication_s'] == "1")
		$ret .=  ' Conférence invitée. ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationBrevet($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s']))
		$ret .= $notice['subTitle_s'][0].". ";
		
	if (isset($notice['number_s']))
		$ret .= "(Brevet n°: ".$notice['number_s'][0]."). ";
		
	if (isset($notice['country_s'])) 
		$ret .=  code_to_country($notice['country_s']).", ";
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationRapport($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .= $notice['subTitle_s'][0].". ";
		
	if (isset($notice['authorityInstitution_s'])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."), ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['number_s']))
		$ret .= $notice['number_s'][0].', ';
		
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationChapitreRapport($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';
		
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationPrepubli($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['serie_s'][0]))
		$ret .=  $notice['serie_s'][0] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 	
	
	if (isset($notice['arxivId_s']))
		$ret .= "n° arxiv: ".$notice['arxivId_s'].", ";
		
	if (isset($notice['biorxivId_s']))
		$ret .= "n° biorxiv: ".$notice['biorxivId_s'][0].", ";	
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationTheseHdrMemoire($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0]." ";
	
	if (isset($notice['authorityInstitution_s'][0])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."). ";
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationCours($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0]." ";
	
	if (isset($notice['lectureName_s']) || isset($notice['lectureType_s']) || isset($notice['authorityInstitution_s'])){
		$cours = "(". $notice['lectureName_s'] .", ". getLectureType($notice['lectureType_s']) .", ". $notice['authorityInstitution_s'][0] .")";
		$cours = str_replace('(, ','(',$cours);
		$cours = str_replace(', )',')',$cours);
		$ret .= $cours.", ";
	}
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationLogiciel($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationImageCarte($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['country_s'])) 
		$ret .= code_to_country($notice['country_s']).", ";
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationSonVideo($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";
	
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
} 

function getCitationAutre($notice){
	$ret = "";
	$ret .= getAuteursHceres($notice). ' ';
	
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	
	$ret .= $notice['title_s'][0].". ";
	
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	
	if (isset($notice['authorityInstitution_s'][0])) 
		$ret .= "(".$notice['authorityInstitution_s'][0]."). ";
	
	if (isset($notice['country_s'])) 
		$ret .= code_to_country($notice['country_s']).", ";
	
	if (isset($notice['serie_s'][0]))
		$ret .=  $notice['serie_s'][0] . ', ';
	
	if (isset($notice['journalTitle_s']))
		$ret .= $notice['journalTitle_s'].", ";
	
	if (isset($notice['bookTitle_s']))
		$ret .=  "In : ".$notice['bookTitle_s'] . '. ';	
	
	if (isset($notice['lectureName_s']) || isset($notice['lectureType_s']) || isset($notice['authorityInstitution_s'])){
		$cours = "(". $notice['lectureName_s'] .", ". getLectureType($notice['lectureType_s']) .", ". $notice['authorityInstitution_s'][0] .")";
		$cours = str_replace('(, ','(',$cours);
		$cours = str_replace(', )',')',$cours);
		$ret .= $cours.", ";
	}
		
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';	
	
	if (isset($notice['arxivId_s']))
		$ret .= "n° arxiv: ".$notice['arxivId_s'].", ";
		
	if (isset($notice['biorxivId_s']))
		$ret .= "n° biorxiv: ".$notice['biorxivId_s'][0].", ";	
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'].', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", ";	

	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

/* ajout des fonctions de citations liés aux nouveaux types hal 2022 */
function getCitationCommune($notice){
	$ret = getAuteursHceres($notice). ' ';
	if (isset($notice['producedDate_s']))
		$ret .= "(".$notice['producedDate_s']."). ";
	$ret .= $notice['title_s'][0].". ";
	if (isset($notice['subTitle_s'])) 
		$ret .=  $notice['subTitle_s'][0].". ";
	return $ret;
}

function getCitationProceeding($notice){
	$ret = getCitationCommune($notice);
	if (isset($notice['publisher_s'][0]))
		$ret .=  $notice['publisher_s'][0] . ', ';
	
	if (isset($notice['page_s']))
		$ret .= $notice['page_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitationBlogTrad($notice){
	$ret = getCitationCommune($notice);
	if (isset($notice['description_s']))
		$ret .= $notice['description_s'] . ', ';
	
	if (isset($notice['doiId_s'])) 
		$ret .= $notice['doiId_s'].", "; 
		
	$lien = changeUri($notice);
	$ret .= $lien;
	
	return $ret;
}

function getCitation($notice){
	switch($notice['docType_s']){
		case 'ART':		  return getCitationArticle($notice);							break;
		case 'REPORT':    return getCitationRapport($notice);    						break;
		case 'OUV':		  return getCitationOuvrage($notice);							break;
		case 'DOUV':      return getCitationDirectionOuvrage($notice);		            break;
		case 'COUV':      return getCitationChapitreOuvrage($notice);					break;
		case 'COMM':      return getCitationCommunication($notice);						break;
		case 'HDR':		  return getCitationTheseHdrMemoire($notice);					break;					
		case 'OTHER':	  return getCitationAutre($notice);								break;
		case 'SOFTWARE':  return getCitationLogiciel($notice);							break;
		case 'PATENT':    return getCitationBrevet($notice);							break;
		case 'LECTURE':   return getCitationCours($notice);								break;
		case 'VIDEO':     return getCitationSonVideo($notice);							break;
		case 'MAP':       return getCitationImageCarte($notice);						break;
		case 'IMG':       return getCitationImageCarte($notice);						break;
		case 'SON':       return getCitationSonVideo($notice);							break;
		case 'POSTER':    return getCitationCommunication($notice);						break;
		case 'MEM':       return getCitationTheseHdrMemoire($notice);					break;
		case 'CREPORT':   return getCitationChapitreRapport($notice);					break;
		case 'THESE':     return getCitationTheseHdrMemoire($notice);					break;
		case 'UNDEFINED': return getCitationPrepubli($notice);							break;
		/* nouveaux type std 2022 */
		case 'MEMLIC':      return getCitationTheseHdrMemoire($notice);					break;
		case 'PROCEEDINGS': return getCitationProceeding($notice);  					break;
		case 'ISSUE':	    return getCitationArticle($notice);      					break;
		case 'TRAD':	    return getCitationBlogTrad($notice);						break;
		case 'BLOG':	    return getCitationBlogTrad($notice);						break;
		case 'NOTICE':		return getCitationChapitreOuvrage($notice);					break;
		default :         	return getCitationArticle($notice);                          break;
	}		
}

function getSousType($notice){
	switch($notice['docType_s']){
		case 'ART':		  if (isset($notice['inra_otherType_Art_local_s']) )   	return ssTypeArticle($notice['inra_otherType_Art_local_s'][0]);						break;
		case 'REPORT':    if ($notice['inra_reportType_local_s'][0] == "RE")
							return "Rapport d'expertise";
						  if (isset($notice['reportType_s']))
							return ssTypeRapportStandard($notice['reportType_s']);
		break;
		case 'OUV':		  if (isset($notice['inra_otherType_Ouv_local_s']) ) 	return ssTypeOuv($notice['inra_otherType_Ouv_local_s'][0]);							break;
		case 'DOUV':      if (isset($notice['inra_otherType_Douv_local_s']) ) 	return ssTypeDouv($notice['inra_otherType_Douv_local_s'][0]);		            	break;
		case 'COMM':      if (isset($notice['inra_otherType_Comm_local_s']) )  	return ssTypeComm($notice['inra_otherType_Comm_local_s'][0]);						break;
		case 'SOFTWARE':  if (isset($notice['inra_developmentType_local_s']) ) 	return ssTypeDevelopment($notice['inra_developmentType_local_s'][0]);				break;
		case 'UNDEFINED': if (isset($notice['inra_otherType_Undef_local_s']) )  return ssTypeUndef($notice['inra_otherType_Undef_local_s'][0]);						break;
		default :         return "";  break;
	}
	return "";	
}

?>
