<?php 


$tabres = "<table class=\"table table-striped\">                
                <tbody>";
	
	$nb = count($articles);
	if ( $nb > 0 ) {
		$msg .= "<br/>Articles : ".$nb;
		$labels[] = 'Articles';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(255, 99, 132)';
		$tabres .= "<tr><th>Articles</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($comms);	
	if ( $nb > 0 ) {
		$msg .= "<br/>Communications : ".$nb;
		$labels[] = 'Communications';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(221, 221, 17)';
		$tabres .= "<tr><th>Communications</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($posters);
	if ( $nb > 0 ) {
		$msg .= "<br/>Posters : ".$nb;
		$labels[] = 'Posters';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(221, 255, 170)';
		$tabres .= "<tr><th>Posters</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($proceedings);
	if ( $nb > 0 ) {
		$msg .= "<br/>Proceedings : ".$nb;
		$labels[] = 'Proceedings';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(221, 255, 17)';
		$tabres .= "<tr><th>Proceedings</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($issues);
	if ( $nb > 0 ) {
		$msg .= "<br/>N° spécial de revue : ".$nb;
		$labels[] = 'N° spécial de revue';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(238, 34, 187)';
		$tabres .= "<tr><th>N° spécial de revue</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($ouvrages);
	if ( $nb > 0 ) {
		$msg .= "<br/>Ouvrages : ".$nb;
		$labels[] = 'Ouvrages';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(238, 170, 17)';
		$tabres .= "<tr><th>Ouvrages</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($chapitres);
	if ( $nb > 0 ) {
		$msg .= "<br/>Chapitres d'ouvrage : ".$nb;
		$labels[] = 'Chapitres d\'ouvrage';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(17, 0, 102)';
		$tabres .= "<tr><th>Chapitres d'ouvrage</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($blogs);
	if ( $nb > 0 ) {
		$msg .= "<br/>Articles de blog scientifique : ".$nb;
		$labels[] = 'Articles de blog scientifique';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(0, 238, 238)';
		$tabres .= "<tr><th>Articles de blog scientifique</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($encyclos);
	if ( $nb > 0 ) {
		$msg .= "<br/>Not. encyclopédie / dictionnaire : ".$nb;
		$labels[] = 'Not. encyclopédie / dictionnaire';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(0, 187, 136)';
		$tabres .= "<tr><th>Not. encyclopédie / dictionnaire</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($trads);
	if ( $nb > 0 ) {
		$msg .= "<br/>Traductions : ".$nb;
		$labels[] = 'Traductions';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(102, 51, 102)';
		$tabres .= "<tr><th>Traductions</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($brevets);
	if ( $nb > 0 ) {
		$msg .= "<br/>Brevets : ".$nb;
		$labels[] = 'Brevets';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(68, 68, 0)';
		$tabres .= "<tr><th>Brevets</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($others);
	if ( $nb > 0 ) {
		$msg .= "<br/>Autres publications : ".$nb;
		$labels[] = 'Autres publications';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(68, 170, 187)';
		$tabres .= "<tr><th>Autres publications</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($prepublis);
	if ( $nb > 0 ) {
		$msg .= "<br/>Preprint, Working Paper : ".$nb;
		$labels[] = 'Preprint, Working Paper';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(68, 136, 136)';
		$tabres .= "<tr><th>Preprint, Working Paper</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($rapports);
	if ( $nb > 0 ) {
		$msg .= "<br/>Rapports : ".$nb;
		$labels[] = 'Rapports';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(85, 0, 102)';
		$tabres .= "<tr><th>Rapports</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($chapitresRapport);
	if ( $nb > 0 ) { 
		$msg .= "<br/>Chapitres de rapport : ".$nb;
		$labels[] = 'Chapitres de rapport';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(170, 68, 34)';
		$tabres .= "<tr><th>Chapitres de rapport</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($theses);	
	if ( $nb > 0 ) {
		$msg .= "<br/>Thèses : ".$nb;
		$labels[] = 'Thèses';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(187, 0, 0)';
		$tabres .= "<tr><th>Thèses</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($hdrs);
	if ( $nb > 0 ) {
		$msg .= "<br/>HDR : ".$nb;
		$labels[] = 'HDR';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(153, 221, 255)';
		$tabres .= "<tr><th>HDR</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($memoires);
	if ( $nb > 0 ) {
		$msg .= "<br/>Mémoires : ".$nb;
		$labels[] = 'Mémoires';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(0, 221, 255)';
		$tabres .= "<tr><th>Mémoires</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($cours);
	if ( $nb > 0 ) {
		$msg .= "<br/>Cours : ".$nb;
		$labels[] = 'Cours';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(221, 0, 120)';
		$tabres .= "<tr><th>Cours</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($images);
	if ( $nb > 0 ) {
		$msg .= "<br/>Images : ".$nb;
		$labels[] = 'Images';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(119, 238, 204)';
		$tabres .= "<tr><th>Images</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($sons);
	if ( $nb > 0) {
		$msg .= "<br/>Sons : ".$nb;
		$labels[] = 'Sons';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(119, 85, 17)';
		$tabres .= "<tr><th>Sons</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($videos);
	if ( $nb > 0) {
		$msg .= "<br/>Vidéos : ".$nb;
		$labels[] = 'Vidéos';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(136, 34, 200)';
		$tabres .= "<tr><th>Vidéos</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($cartes);
	if ( $nb > 0) {
		$msg .= "<br/>Cartes : ".$nb;
		$labels[] = 'Cartes';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(136, 170, 34)';
		$tabres .= "<tr><th>Cartes</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($logs);
	if ( $nb > 0 ) {
		$msg .= "<br/>Logiciels : ".$nb;
		$labels[] = 'Logiciels';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(136, 136, 180)';
		$tabres .= "<tr><th>Logiciels</th><th>".$nb."</th></tr>";
	}
	
	$nb = count($autres);
	if ( $nb > 0 ) { 
		$msg .= "<br/>Non classés : ".$nb;
		$labels[] = 'Non classés';
		$nbNotices[] = $nb;
		$couleurs[] = 'rgb(136, 136, 255)';
		$tabres .= "<tr><th>Non classés</th><th>".$nb."</th></tr>";
	}
	
	/* TOTAL */
	$tabres .= "<tr><th><b>TOTAL</b></th><th><b>".$nbNoticeTraite."</b></th></tr>";
	
	$tabres .= "</tbody></table>";
	
?>