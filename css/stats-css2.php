<?php
/*************************************************************************************/
/* Pierre Pichard - 19/10/23
/* Script de comptage des exports CSS réalisé entre le 28 juin et le 11 octobre inclus  
1- Nombre d'Utilisations
2- Nombre d'utilisateurs distincts
3- Nombre moyen d'Utilisations
4- Tableau du nombre d'export par idHal 
**************************************************************************************/


 
error_reporting(E_ALL);
ini_set('display_errors', '1');

$path = "docs/";
$tab = array();
$tabId = array();
$tabCpt = array();
if ($handle = opendir($path)) {
    $i=0;
	while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $idHal = explode("_",$entry)[0];
			$mois =  (int)(date ("n", filemtime($path.$entry)));  // mois de 1 à 12
			$jour =  (int)(date ("j", filemtime($path.$entry)));
			
						
			if ( ($mois==6 && $jour > 28) || $mois==7 || $mois==8 || $mois==9 || ($mois==10 && $jour < 12)) {
				
			    $tabId[$idHal] = $entry;
				$tab[] = $idHal;
	
			}
        }
    }
    closedir($handle);
	
	$nbUsers = count($tabId);
	$nbUses = count($tab);
	$tabCpt = array_count_values($tab);
	echo '<h2>'.$nbUsers.' Utilisateurs</h2>';
	echo '<h2>'.$nbUses.' Utilisations</h2>';
	
	echo '<h2>'.round($nbUses / $nbUsers,2).' Moy. utilisations</h2>';
	
	
	
	echo '<br><br>Nombre d\'exports / idHal :<br><pre>';
	print_r($tabCpt);
	echo '</pre>';
	
	$fp = fopen('bilan.csv', 'w'); 
	  
	 
	foreach ($tabCpt as $nb => $idHal)  
	   fputcsv($fp,array($nb,$idHal),";");
	 
	   
	fclose($fp);

}


?>