

function validYear(testDate){
	if (testDate > 1900 &&  testDate<2023)
		return true;
	else
		return false;
}

function efface(formeBool){
	$('#resultats').html(' ');
	$('#erreurs').html(' ');
	$('#details').html(' ');
	$('#dlbouton').hide();
	$('#detailsbouton').hide();
	if (formeBool) $("#formeauteur").prop("checked", false);
	$('#libelleBouton').hide();
	$('#tableauresultat').html(' ');
}


function lance_recherche(){
	// on commence par effacer les précédents resultats
	efface(false);
	
	var idhal = $('#idhal').val().trim();
	var dated = $('#datedeb').val().trim();
	var datef = $('#datefin').val().trim();
	var forme;
	// vidange des zones avant nouvel export : todo
	if ($('#formeauteur').is(':checked')) forme=1; else forme=0;
	
	var datedeb;
	if (dated === "") datedeb = 1900; else datedeb = parseInt(dated,10);
	
	var datefin;
	if (datef === "") datefin = 2023; else datefin = parseInt(datef,10);
	
	if (datedeb < 1900 ||  datedeb > 2023) {
		$('#erreurs').html("Veuillez SVP saisir une date début valide<br />");
		return ;
	}
	if (datefin < 1900 ||  datefin > 2023) {
		$('#erreurs').append("Veuillez SVP saisir une date de fin valide<br />");
		return ;
	}
	
	if (datedeb > datefin) {
		$('#erreurs').append("La date de début doit être inférieure ou égale à la date de fin<br />");
		return ;
	}
	
	var words = idhal.split(' ');
	if (words.length>1) {
		$('#erreurs').append("idHal invalide : Veuillez saisir une chaine de caratères continue SVP.<br />");
		return ;
	}
	
	if (idhal === "") {
		$('#erreurs').append("idHal vide : Veuillez saisir votre idHAL SVP.<br />");
		return ;
	}
	
	// affichage de "Export en cours, veuillez patienter SVP ..."
	$("#chargement").html("Export en cours, veuillez patienter SVP <img src=\"./assets/img/loading.gif\"/>").show(); 
	
	if (typeof myChart != 'undefined')
		myChart.destroy();
		

	$.ajax({
		url: '../../css/evalcss.php',
		type: 'POST',
		data: 'idHal=' + idhal + '&dateDeb=' + datedeb + '&dateFin=' + datefin + '&formeAut='+forme,
		dataType: 'JSON',
		success: function(data, status, xhr){
			$("#chargement").hide();
			if (data[2]!="Erreur") {
							
				
				url_fichier = 'https://' + window.location.host + data[2];
				$('#details').html('<a href="' + url_fichier +'" id="btnresultat" target="_blank" class="btn btn-label btn-success"><label><i class="fa fa-download"></i></label>TELECHARGER L\'EXPORT</a><br><br>'); 
			
				$('#details').append(data[0]);
				$('#resultats').show();
				$('#details').show();
							          
								
				ctx = document.getElementById('monDonut');

				data_donut = {
				  labels: data[3],
				  datasets: [{
					label: 'Nombre de publications',
					data: data[4],
					backgroundColor: data[5],
					hoverOffset: 4
				  }]
				};
		
				
				if (typeof myChart != 'undefined') myChart.destroy();
					myChart = new Chart(ctx, {
						type: 'doughnut',
						data: data_donut
				});
				
				
				$('#donutbox').show();
				$('html').css("overflow-y","auto");  // affichage de l'ascenseur
				$('footer').css("position","relative");
				
				$('html, body').animate({
					scrollTop: $("#submitbouton").offset().top
				}, 1500);
				
			
				$('#tableauresultat').html(data[6]);                 // #resultats : zone d'affichage des stats en partie gauche sous le donut
				
				
			} else {
				$('#erreurs').html(data[1]);
				$('#erreurs').show();
			}
			//console.log(data);
		},
		error: function(jqXhr, textStatus, errorMessage){
			$('#resultats').html(errorMessage);
			$('#resultats').show();
		}
	});
	
}

$(document).ready(function(){	
	
	// au chargement de la page on cache le bouton de résultat
	$("#bouton_resultat").hide();
	$('#dlbouton').hide();
	$('#libelleBouton').hide();
	
	// on appuie sur la touche Entrée
	$('body').keypress(function(e){
		if( e.which == 13 ){
			lance_recherche();
		}
	});
	
	
	// Lorsqu'on valide le formulaire
	$("#submitbouton").click(function(){
		lance_recherche();		
	}) 
		
	
	// clic sur le bouton ANNULER
	$("#cancelbouton").click(function(){
		$('#idhal').val('');
		$('#datedeb').val('');
		$('#datefin').val('');
		$("#bouton_resultat").hide();
		$('#dlbouton').hide();
		$('#detailsbouton').hide();
		$('#resultats').html(' ');
		$('#resultats').hide();
		$('#details').html(' ');
		$('#erreurs').html(' ');
		$("#formeauteur").prop("checked", false);
		if (typeof myChart != 'undefined')
			myChart.destroy();
		$("#tableauresultat").html('');
	})
	
	// clic sur le bouton Resultats de l'export
	$("#detailsbouton").click(function(){
		$('#resultats').show();
	})
	
	
	var $videoSrc;  
	$('.video-btn').click(function() {
		$videoSrc = $(this).data( "src" );
	});
	//console.log($videoSrc);

  
	// when the modal is opened autoplay it  
	$('#myModal').on('shown.bs.modal', function (e) {
		
	// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
	$("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
	})
	  


	// stop playing the youtube video when I close the modal
	$('#myModal').on('hide.bs.modal', function (e) {
		// a poor man's stop video
		$("#video").attr('src',$videoSrc); 
	}) 

	
});