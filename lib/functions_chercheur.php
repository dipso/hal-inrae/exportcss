<?php
/*******************   functions_chercheur.php
/// fonctions utiles à l'export de type 'Chercheur'
/// la plupart de ces fonctions sont déjà présentes pour l'export collection qui est trés similaire
/// mais le choix a été fait de dupliquer cet export pour le personnaliser par la suite
/// Pierre Pichard  20/09/2022
/// MAJ : 12/2022 : Prise en compte des nouveaux types de documents et sous-type standards HAL
*/

// Fonction permettant d'affecter à un numéro de colonne dans l'export une valeur
// Ex : dans la première colonne, on place le type de document
// $colomn : numéro de la colonne dans l'export excel à traiter
// $notice : données de la notice en cours de traitement
// $chpSuppl : champs supplementaires ajouter par l'utilisateur (mots cles fr ou en, resumé fr ou en) à intercaler entre les colonnes 34 et 37
function getValue($column,$notice,$chpSuppl){
	$nbChpEnPlus = count($chpSuppl);
	switch($column){
		case '0':  return getTypeDoc($notice['docType_s']);		          						break;
		/* 12/22 : on ajoute le nouveau sousTypeStd de Hal */
		case '1':  return getSousTypeDoc($notice['docSubType_s']);		          				break;
		case '2':  return getAuteursSoulign($notice,$soulignAuteur,$equipes);		          	break;
		case '3':  return $notice['title_s'][0];		         								break;
		case '4':  return $notice['journalTitle_s'];		          							break;
		case '5':  return $notice['volume_s'];		          							     	break;   /* correction le 10/11/23 */
		case '6':  return implode(' ',$notice['issue_s']);		          						break;
		case '7':  return $notice['page_s'];		          									break;
		case '8':  return getAnnee($notice);		          									break;
		case '9':  return $notice['doiId_s'];		          									break;
		case '10':  return getOpenAcess($notice);		          								break;
		case '11': return getPublicVise($notice['inra_publicVise_local_s'][0]);		          	break;
		case '12': return getComite($notice);		          									break;
		case '13': return getVulgarisation($notice);		          							break;
		case '14': return $notice['conferenceTitle_s'];		          							break;
		case '15': return $notice['city_s'];		          									break;
		case '16': return code_to_country($notice['country_s']);		          				break;
		case '17': return $notice['conferenceStartDate_s'];		          						break;
		case '18': return $notice['conferenceEndDate_s'];		          						break;
		case '19': return getAudience($notice);		          									break;
		case '20': return getConferenceInvitee($notice);	          							break;
		case '21': return $notice['bookTitle_s'];		          								break;
		case '22': return $notice['serie_s'][0];		          								break;
		case '23': return $notice['publisher_s'][0];		          							break;
		case '24': return $notice['lectureName_s'];		          								break;
		case '25': return getLectureType($notice['lectureType_s']);		          				break;
		case '26': return $notice['wosId_s'][0];		          								break;
		case '27': return " ".$notice['arxivId_s']." ";											break;  //,PHPExcel_Style_NumberFormat::FORMAT_TEXT
		case '28': return $notice['biorxivId_s'][0];		          							break;
		case '29': return $notice['pubmedId_s'];		          								break;
		case '30': return getLangueHceres($notice);		          								break;
		case '31': 
			if ($nbChpEnPlus == 0)
				return $notice['halId_s'];
			if ($nbChpEnPlus > 0) {
				$champ = $chpSuppl[0];  // si on a au moins un champ en plus alors en 35 on affiche le premier
				if ($champ == "fr_abstract_s" || $champ == "fr_abstract_s")
					return $notice[$champ][0];
				else return implode($notice[$champ]," ; ");
			}	
		break;
		
		
		case '32': 		          						
			if ($nbChpEnPlus == 0)
				return changeUri($notice);
			if ($nbChpEnPlus > 1) {
				$champ = $chpSuppl[1];  // si on a au moins 2 champs en plus alors en 36 on affiche le deuxieme
				if ($champ == "fr_abstract_s" || $champ == "fr_abstract_s")
					return $notice[$champ][0];
				else return implode($notice[$champ]," ; ");
			}
		break;
		
		
		case '33': 
			if ($nbChpEnPlus == 0) return getCitation($notice);	
			if ($nbChpEnPlus == 1) return changeUri($notice);   // on retourne l'avant dernier 
			if ($nbChpEnPlus == 2) return $notice['halId_s']; 
			if ($nbChpEnPlus  > 2) {
				$champ = $chpSuppl[2];  // si on a au moins 3 champs en plus alors en 37 on affiche le deuxieme
				if ($champ == "fr_abstract_s" || $champ == "fr_abstract_s")
					return $notice[$champ][0];
				else return implode($notice[$champ]," ; ");
			}
		break;
		
		case '34':
		    if ($nbChpEnPlus == 3) return $notice['halId_s'];  // 38-3=35 soit $notice['halId_s']
			if ($nbChpEnPlus == 2) return changeUri($notice);
			if ($nbChpEnPlus == 1) return getCitation($notice);	
			if ($nbChpEnPlus  > 3) {
				$champ = $chpSuppl[3];  // si on a au moins 3 champs en plus alors en 37 on affiche le deuxieme
				if ($champ == "fr_abstract_s" || $champ == "fr_abstract_s")
					return $notice[$champ][0];
				else return implode($notice[$champ]," ; ");
			}
			return "";
		break;
		
		case '35': 
			if ($nbChpEnPlus == 2) return getCitation($notice);
			if ($nbChpEnPlus == 3) return changeUri($notice);
			if ($nbChpEnPlus == 4) return $notice['halId_s'];
			return "";
		break;
		case '36':
			if ($nbChpEnPlus == 3) return getCitation($notice);
			if ($nbChpEnPlus == 4) return changeUri($notice);
			return "";
		break;
		
		case '37': return getCitation($notice);
		break;
		
		default :  return "";  break;
	}
}


// chaine avant : ,'Langue du document',fr_keyword_s,en_abstract_s,'hal-id',
// chaine retournée : ,'Langue du document','Mots clés français','Résumé en anglais','hal-id',
function calculLibelles($libelles) {
   $libelles = str_replace("fr_keyword_s" ,"Mots-clés français",$libelles);
   $libelles = str_replace("en_keyword_s" ,"Mots-clés anglais",$libelles);
   $libelles = str_replace("en_abstract_s","Résumé anglais",$libelles);
   $libelles = str_replace("fr_abstract_s","Résumé français",$libelles);
   return $libelles;
}

?>