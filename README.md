# Portail d’exports INRAE des données de HAL

Cet outil permet de réaliser l'export des publications présentes dans HAL pour les évaluations CSS des chercheurs. Il permet également à un chercheur d'exporter ses publications au format Excel idem pour l'export "Collection" qui permet d'exporter toutes les publications d'une collection donnée (et selon un intervalle d'année donné). Le portail propose également un renvoi vers l'export HCERES officiel 

INSTALLATION : copie des fichiers dans un répertoire web dédié

MISE EN MAINTENANCE : Renommer la page index.html en index2.html par exemple et renommer la page index-m.html en index.html pour mettre en plcae la page de "Mise en maintenance" du portail.  

USAGE : une page d'acceuil propose 4 boutons un pour l'export CSS et l'autre pour l'export HCERES

REQUIS : installation de php-xml et php-zip

CSS : l'utilisateur rempli le formulaire puis obtient un fichier RTF contenant les publications liées à l'idHal saisi dans le formulaire.



/* Modifications et corrections récentes */
/* 10/11/2023 : correction du message d'erreur si mauvais idhal ou code collection saisi  */
/* 10/11/2023 : correction du dans l'export de collection et chercheur pour le champ volume_s (chaine de caractère au lieu de tableau de valeurs */
/* 10/11/2023 : ajout du programme de comptage des exports produits à des fins statistiques : css/stats-css2.php   */

