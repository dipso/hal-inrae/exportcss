<?php
/*******************************************************************************************************************************************/
/*
/*              PROGRAMME D'EXPORT HCERES POUR L'INRAE
/*
/*   Usage : appel ajax depuis assets/js/hceres.js
/*   Paramatres : collection, année de début, année de fin, booléen soulignement des auteurs et info labo (pour le soulignement des auteurs)
/*   Résultat : production d'un fichier excel dans le répertoire xlsx/ comportant les notices résultantes de l'interrogation de l'API HAL
/*              avec ventilation par onglet pour chaque type de publication
/*   Développement : Pierre Pichard - wouaib.com
/*   Date : 04/2022
/*
/*******************************************************************************************************************************************/


ini_set('memory_limit', '292M');
$debug = false;
if ($debug)
	$h = fopen("debug.log","w");
/** Error reporting */
error_reporting(E_ERROR);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


$noticesTraitees = array();  // stockage de toutes les publis traitées, pour remplir la section des publis non ventilées
$nbNoticeTraite=0;
$liste1 = "halId_s,title_s,authFullName_s,authLastName_s,authFirstName_s,producedDate_s,producedDateY_i,docType_s,docSubType_s,inra_publicVise_local_s,peerReviewing_s,invitedCommunication_s,subTitle_s,bookTitle_s,journalTitle_s,volume_s,issue_s,page_s,publisher_s,doiId_s,uri_s,arxivId_s,biorxivId_s,authorityInstitution_s,number_s,serie_s,conferenceTitle_s,city_s,country_s,conferenceStartDate_s,conferenceEndDate_s,lectureName_s,reportType_s,lectureType_s,submitType_s,openAccess_bool,wosId_s,pubmedId_s,audience_s,otherType_s,authQuality_s,authIdFullName_fs";
$liste2   = "inra_otherType_Other_local_s,inra_otherType_Art_local_s,inra_otherType_Undef_local_s,inra_otherType_Douv_local_s,inra_otherType_Comm_local_s,inra_reportType_local_s,popularLevel_s,authIdHasPrimaryStructure_fs,linkExtId_s,language_s";
$listeChamps = $liste1.','.$liste2;


// Récuperation des données du formulaire
$collection    = htmlspecialchars(strip_tags($_POST['idcoll']),    ENT_QUOTES);
$anneedeb      = htmlspecialchars(strip_tags($_POST['dateDeb']),  ENT_QUOTES);
$anneefin      = htmlspecialchars(strip_tags($_POST['dateFin']),  ENT_QUOTES);
$soulignAuteur = htmlspecialchars(strip_tags($_POST['soulignauteur']), ENT_QUOTES);  // 1 pour cocher, 0 sinon
// id AuréHAL, le nom ou l'acronyme de votre unité, selon que vous souhaitez mettre en évidence le nom des auteurs de l'unité.  Exemple 928~ECOBIO~575446
$equipeLabo    = htmlspecialchars(strip_tags($_POST['equipelabo']), ENT_QUOTES);

if (isset($collection))
	$collection=strtoupper($collection);

if ($equipeLabo == "")
	$equipeLabo = $collection;

@include_once("../lib/functions.php");
@include_once("../lib/functions_hceres.php");

if (isset($collection) && isset($anneedeb) && isset($anneefin)) {
  
	$url = "http://api.archives-ouvertes.fr/search/?wt=json&q=collCode_s:".$collection."&rows=100000&fq=producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&fl=".$listeChamps;
	$linkHAL = '<a href="'. $url. '" target="_blank">Afficher la requête API (format json)</a>';
	if ($anneedeb=="") $annedeb="*";
	if ($anneefin=="") $anneefin="*";
	$urlHAL = "https://hal.inrae.fr/search/index/?q=collCode_s:".$collection."+producedDateY_i:[".$anneedeb."+TO+".$anneefin. "]&sort=producedDateY_i%20desc&submit=&docType_s=ART+OR+COMM+OR+POSTER+OR+OUV+OR+COUV+OR+DOUV+OR+PATENT+OR+OTHER+OR+UNDEFINED+OR+REPORT+OR+CREPORT+OR+THESE+OR+HDR+OR+LECTURE+OR+MEM+OR+VIDEO+OR+SON+OR+IMG+OR+MAP+OR+SOFTWARE&submitType_s=notice+OR+file+OR+annex&rows=300";
	$linkInrae = '<a href="'. $urlHAL. '" target="_blank">Lancer la recherche sur HAL - INRAE</a>';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if (isset ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
	}
	$resultats = curl_exec($ch);
	curl_close($ch);
	
	$data = json_decode($resultats,true);
	$notices = $data["response"]["docs"];
		
	if (count($notices)==0) {
		$retourne[1] = "Aucune notice ne correspond à ces critères. Veuillez saisir un code de collection correct SVP.";
		$retourne[2] = "Erreur";
		$error = true;
		echo json_encode($retourne);
		exit;
	} else {
		$msg = count($notices)." notices correspondent aux critères";
		$error = false;
	}
	
	// si Identifiant structure (($equipeLabo) saisi est "1002209~AGIR" alors $equipes = 1002209~574738~74637~100299~560019~1002209~552012~225605~531394
	$equipes = getAffiliations($equipeLabo);  // on récupere toutes les valeurs à comparer avec les auteurs des notices, liste de docid de structure 928~1050~...
	if ($equipes == "")
		$msg .= " Pas de structures";
	
	if ($debug){
		fwrite($h,"Equipes : ");
		fwrite($h,print_r($equipes,true));
		fwrite($h,"\nSoulignement: ".$soulignAuteur);
	}
	
	$dir = dirname(__FILE__);
	
	/** Include PHPExcel */
	require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

	// Create new PHPExcel object
	//echo date('H:i:s') , " Create new PHPExcel object" , EOL;
	$workbook  = new PHPExcel();
	
	// Ventilation par type de document
	foreach ($notices as $notice) {
		switch($notice['docType_s']){
			case 'ART':		  $articles[]  = $notice;		break;
			case 'REPORT':    $rapports[]  = $notice;       break;
			case 'CREPORT':   $chapitresRapport[]= $notice; break;
			case 'OUV':		  $ouvrages[]  = $notice;		break;
			//case 'DOUV':      $douvs[]     = $notice;		break;
			case 'DOUV':      $ouvrages[]     = $notice;		break;
			// Les directions d'ouvrage ne répondant pas aux critères ci-dessus sont devenues des ouvrages		
			case 'COUV':      $chapitres[] = $notice;		break;
			case 'COMM':      $comms[]     = $notice;		break;
			case 'HDR':		  $hdrs[]      = $notice;		break;					
			case 'OTHER':	  $others[]    = $notice;		break;   // Autre publication scientifique
			case 'SOFTWARE':  $logs[]      = $notice;		break;
			case 'PATENT':    $brevets[]   = $notice;		break;
			case 'LECTURE':   $cours[]     = $notice;		break;
			case 'VIDEO':     $videos[]    = $notice;		break;
			case 'MAP':       $cartes[]    = $notice;		break;
			case 'IMG':       $images[]    = $notice;		break;
			case 'SON':       $sons[]      = $notice;		break;
			case 'POSTER':    $posters[]   = $notice;		break;
			case 'UNDEFINED': $prepublis[] = $notice;		break;
			case 'THESE':	  $theses[]    = $notice;		break;
			case 'MEM':	      $memoires[]  = $notice;		break;
			case 'MEMLIC':	  $memoires[]  = $notice;		break;
			/* Nouveaux types 12/2022 */
			case 'PROCEEDINGS': $proceedings[] = $notice;		break;      // Proceedings/ recueil de communications
			case 'ISSUE':	    $issues[]      = $notice;		break;		// N° spécial de revue/ special issue
			case 'TRAD':	    $trads[]       = $notice;		break;		// Traduction
			case 'BLOG':	    $blogs[]       = $notice;		break;      // Article de blog scientifique
			case 'NOTICE':	    $encyclos[]    = $notice;		break;      // Notice d'encyclopédie ou de dictionnaire
			default :           $autres[]      = $notice;		break;
		}
	}
	
	// ***********************************************************//
	// Remplissage du document Excel
	//
	////////////////////////////////////////////////////////////////
	// ****************** ARTICLES *******************************//
	if (!empty($articles)){
		$sheet = $workbook->getActiveSheet();
		$sheet->setTitle('Article');
		$sheet->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet->setCellValue('C1', 'Revue'                      );    // colonne 2
		$sheet->setCellValue('D1', 'Volume'                     );    // colonne 3	
		$sheet->setCellValue('E1', 'Numéro'                     );    // colonne 4
		$sheet->setCellValue('F1', 'Pages'                      );    // colonne 5
		$sheet->setCellValue('G1', 'Année'                      );    // colonne 6
		$sheet->setCellValue('H1', 'DOI'                        );    // colonne 7
		$sheet->setCellValue('I1', 'Equipe'                     );    // colonne 8
		$sheet->setCellValue('J1', 'Doctorant (O/N)'            );    // colonne 9
		$sheet->setCellValue('K1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'           );    // colonne 10
		$sheet->setCellValue('L1', 'OA (O/N)'                   );    // colonne 11
		$sheet->setCellValue('M1', 'Sous-type Article'          );    // colonne 12
		$sheet->setCellValue('N1', 'Vulgarisation (O/N)'        );    // colonne 14
		$sheet->setCellValue('O1', 'Comité de lecture (O/N)'    );    // colonne 15
		$sheet->setCellValue('P1', 'Langue du document'         );    // colonne 16
		$sheet->setCellValue('Q1', 'Clé UT (WOS)'               );    // colonne 17
		$sheet->setCellValue('R1', 'arXiv id'                   );    // colonne 18
		$sheet->setCellValue('S1', 'BioRxiv id'                 );    // colonne 19
		$sheet->setCellValue('T1', 'PubMed id'                  );    // colonne 20
		$sheet->setCellValue('U1', 'hal-id'                     );    // colonne 21
		$sheet->setCellValue('V1', 'Citation'                   );    // colonne 22
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($articles as $article){
			
			$sheet->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($article,$soulignAuteur,$equipes));
			$sheet->setCellValueByColumnAndRow( 1,  $ligne, $article['title_s'][0]);
			$sheet->setCellValueByColumnAndRow( 2,  $ligne, $article['journalTitle_s']);
			$sheet->setCellValueByColumnAndRow( 3,  $ligne, $article['volume_s']);
			if (isset($article['issue_s']))
				$sheet->setCellValueByColumnAndRow( 4, $ligne, implode(' ',$article['issue_s']) );
			$sheet->setCellValueByColumnAndRow( 5,  $ligne, $article['page_s'] );
			$sheet->setCellValueByColumnAndRow( 6,  $ligne, getAnnee($article));
			$sheet->setCellValueByColumnAndRow( 7,  $ligne, $article['doiId_s'] );
			$sheet->setCellValueByColumnAndRow( 8,  $ligne, $article['deptStructAcronym_s'] );
			$sheet->setCellValueByColumnAndRow( 9,  $ligne, "");
			$sheet->setCellValueByColumnAndRow( 10, $ligne, getPremierDernier($article,$equipes) );  // Premier, dernier... O/N  
			$sheet->setCellValueByColumnAndRow( 11, $ligne, getOpenAcess($article)  );   
			$sheet->setCellValueByColumnAndRow( 12, $ligne, getSousTypeDoc($article['docSubType_s']) );
			$sheet->setCellValueByColumnAndRow( 13, $ligne, getVulgarisation($article) );
			$sheet->setCellValueByColumnAndRow( 14, $ligne, getComite($article) );
			$sheet->setCellValueByColumnAndRow( 15, $ligne, getLangueHceres($article));
			$sheet->setCellValueByColumnAndRow( 16, $ligne, $article['wosId_s'][0] );
			$sheet->setCellValueByColumnAndRow( 17, $ligne, " ".$article['arxivId_s']." ",PHPExcel_Style_NumberFormat::FORMAT_TEXT);   // les . ressortent en virgule, tenter le formattage de la cellule
			$sheet->setCellValueByColumnAndRow( 18, $ligne, $article['biorxivId_s'][0] );
			$sheet->setCellValueByColumnAndRow( 19, $ligne, $article['pubmedId_s'] );
			$sheet->setCellValueByColumnAndRow( 20, $ligne, $article['halId_s'] );
			$sheet->setCellValueByColumnAndRow( 21, $ligne, getCitationArticle($article));
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ****************************************************************//
	// ****************** COMMUNICATION *******************************//
	if (!empty($comms)){
		$sheet5 = $workbook->createSheet();
		$sheet5->setTitle('Communication');
		$sheet5->setCellValue('A1', 'Auteur'                      );    // colonne 0
		$sheet5->setCellValue('B1', 'Titre'                       );    // colonne 1
		$sheet5->setCellValue('C1', 'DOI'                         );    // colonne 2
		$sheet5->setCellValue('D1', 'Equipe'                      );    // colonne 3	
		$sheet5->setCellValue('E1', 'Doctorant (O/N)'             );    // colonne 4
		$sheet5->setCellValue('F1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                     );    // colonne 5
		$sheet5->setCellValue('G1', 'OA (O/N)'                    );    // colonne 6
		$sheet5->setCellValue('H1', 'Vulgarisation (O/N)'         );    // colonne 8
		$sheet5->setCellValue('I1', 'Audience'                    );    // colonne 9
		$sheet5->setCellValue('J1', 'Titre du congrès'            );    // colonne 10
		$sheet5->setCellValue('K1', 'Date de début du congrès'    );    // colonne 11
		$sheet5->setCellValue('L1', 'Date de fin du congrès'      );    // colonne 12
		$sheet5->setCellValue('M1', 'Ville du congrès'            );    // colonne 13
		$sheet5->setCellValue('N1', 'Pays du congrès'             );    // colonne 14
		$sheet5->setCellValue('O1', 'Conférence invitée (O/N)'    );    // colonne 15
		$sheet5->setCellValue('P1', 'Langue du document'          );    // colonne 16
		$sheet5->setCellValue('Q1', 'Clé UT (WOS)'                );    // colonne 17
		$sheet5->setCellValue('R1', 'hal-id'                      );    // colonne 18
		$sheet5->setCellValue('S1', 'Citation'                    );    // colonne 19
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($comms as $comm){
			$sheet5->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($comm,$soulignAuteur,$equipes));
			$sheet5->setCellValueByColumnAndRow( 1,  $ligne, $comm['title_s'][0]);
			$sheet5->setCellValueByColumnAndRow( 2,  $ligne, $comm['doiId_s']);
			$sheet5->setCellValueByColumnAndRow( 5,  $ligne, getPremierDernier($comm,$equipes) );  // PPDA
			$sheet5->setCellValueByColumnAndRow( 6,  $ligne, getOpenAcess($comm));
			$sheet5->setCellValueByColumnAndRow( 7,  $ligne, getVulgarisation($comm));
			$sheet5->setCellValueByColumnAndRow( 8,  $ligne, getAudience($comm));
			$sheet5->setCellValueByColumnAndRow( 9, $ligne, $comm['conferenceTitle_s'] );  
			$sheet5->setCellValueByColumnAndRow( 10, $ligne, $comm['conferenceStartDate_s']);  
			$sheet5->setCellValueByColumnAndRow( 11, $ligne, $comm['conferenceEndDate_s']  );
			$sheet5->setCellValueByColumnAndRow( 12, $ligne, $comm['city_s'] );
			$sheet5->setCellValueByColumnAndRow( 13, $ligne, code_to_country($comm['country_s']));
			$sheet5->setCellValueByColumnAndRow( 14, $ligne, getConferenceInvitee($comm));
			$sheet5->setCellValueByColumnAndRow( 15, $ligne, getLangueHceres($comm) );
			$sheet5->setCellValueByColumnAndRow( 16, $ligne, $comm['wosId_s'][0] );
			$sheet5->setCellValueByColumnAndRow( 17, $ligne, $comm['halId_s']);
			$sheet5->setCellValueByColumnAndRow( 18, $ligne, getCitationCommunication($comm));
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}
	
	// ****************************************************************//
	// ****************** POSTER *************************************//
	if (!empty($posters)){
		$sheet6 = $workbook->createSheet();
		$sheet6->setTitle('Poster');
		$sheet6->setCellValue('A1', 'Auteur'                      );    // colonne 0
		$sheet6->setCellValue('B1', 'Titre'                       );    // colonne 1
		$sheet6->setCellValue('C1', 'DOI'                         );    // colonne 2
		$sheet6->setCellValue('D1', 'Equipe'                      );    // colonne 3	
		$sheet6->setCellValue('E1', 'Doctorant (O/N)'             );    // colonne 4
		$sheet6->setCellValue('F1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                     );    // colonne 5
		$sheet6->setCellValue('G1', 'OA (O/N)'                    );    // colonne 6
		$sheet6->setCellValue('H1', 'Vulgarisation (O/N)'         );    // colonne 8
		$sheet6->setCellValue('I1', 'Audience'                    );    // colonne 9
		$sheet6->setCellValue('J1', 'Titre du congrès'            );    // colonne 10
		$sheet6->setCellValue('K1', 'Date de début du congrès'    );    // colonne 11
		$sheet6->setCellValue('L1', 'Date de fin du congrès'      );    // colonne 12
		$sheet6->setCellValue('M1', 'Ville du congrès'            );    // colonne 13
		$sheet6->setCellValue('N1', 'Pays du congrès'             );    // colonne 14
		$sheet6->setCellValue('O1', 'Conférence invitée (O/N)'    );    // colonne 15
		$sheet6->setCellValue('P1', 'Langue du document'          );    // colonne 16
		$sheet6->setCellValue('Q1', 'Clé UT (WOS)'                );    // colonne 17
		$sheet6->setCellValue('R1', 'hal-id'                      );    // colonne 18
		$sheet6->setCellValue('S1', 'Citation'                    );    // colonne 19
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($posters as $poster){
			$sheet6->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($poster,$soulignAuteur,$equipes));
			$sheet6->setCellValueByColumnAndRow( 1,  $ligne, $poster['title_s'][0]);
			$sheet6->setCellValueByColumnAndRow( 2,  $ligne, $poster['doiId_s']);
			$sheet6->setCellValueByColumnAndRow( 3,  $ligne, "");
			$sheet6->setCellValueByColumnAndRow( 4,  $ligne, "");
			$sheet6->setCellValueByColumnAndRow( 5,  $ligne, getPremierDernier($poster,$equipes) );  // PPDA
			$sheet6->setCellValueByColumnAndRow( 6,  $ligne, getOpenAcess($poster));
			$sheet6->setCellValueByColumnAndRow( 7,  $ligne, getVulgarisation($poster));
			$sheet6->setCellValueByColumnAndRow( 8,  $ligne, getAudience($poster));
			$sheet6->setCellValueByColumnAndRow( 9,  $ligne, $poster['conferenceTitle_s'] );  
			$sheet6->setCellValueByColumnAndRow( 10, $ligne, $poster['conferenceStartDate_s']);  
			$sheet6->setCellValueByColumnAndRow( 11, $ligne, $poster['conferenceEndDate_s']  );
			$sheet6->setCellValueByColumnAndRow( 12, $ligne, $poster['city_s'] );
			$sheet6->setCellValueByColumnAndRow( 13, $ligne, code_to_country($poster['country_s']));
			$sheet6->setCellValueByColumnAndRow( 14, $ligne, getConferenceInvitee($poster));
			$sheet6->setCellValueByColumnAndRow( 15, $ligne, getLangueHceres($poster) );
			$sheet6->setCellValueByColumnAndRow( 16, $ligne, $poster['wosId_s'][0] );
			$sheet6->setCellValueByColumnAndRow( 17, $ligne, $poster['halId_s']);
			$sheet6->setCellValueByColumnAndRow( 18, $ligne, getCitationCommunication($poster));
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}	
	

    ////////////////////////////////////////////////////////////////
	// ****************** proceedings *******************************//
	if (!empty($proceedings)){
		$sheet_proc = $workbook->createSheet();
		$sheet_proc->setTitle('Proceedings');
		$sheet_proc->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet_proc->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet_proc->setCellValue('C1', 'Revue'                      );    // colonne 2
		$sheet_proc->setCellValue('D1', 'Volume'                     );    // colonne 3	
		$sheet_proc->setCellValue('E1', 'Numéro'                     );    // colonne 4
		$sheet_proc->setCellValue('F1', 'Pages'                      );    // colonne 5
		$sheet_proc->setCellValue('G1', 'Année'                      );    // colonne 6
		$sheet_proc->setCellValue('H1', 'DOI'                        );    // colonne 7
		$sheet_proc->setCellValue('I1', 'Equipe'                     );    // colonne 8
		$sheet_proc->setCellValue('J1', 'Doctorant (O/N)'            );    // colonne 9
		$sheet_proc->setCellValue('K1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'           );    // colonne 10
		$sheet_proc->setCellValue('L1', 'OA (O/N)'                   );    // colonne 11
		$sheet_proc->setCellValue('M1', 'Vulgarisation (O/N)'        );    // colonne 12
		$sheet_proc->setCellValue('N1', 'Comité de lecture (O/N)'    );    // colonne 13
		$sheet_proc->setCellValue('O1', 'Langue du document'         );    // colonne 14
		$sheet_proc->setCellValue('P1', 'Clé UT (WOS)'               );    // colonne 15
		$sheet_proc->setCellValue('Q1', 'arXiv id'                   );    // colonne 16
		$sheet_proc->setCellValue('R1', 'BioRxiv id'                 );    // colonne 17
		$sheet_proc->setCellValue('S1', 'PubMed id'                  );    // colonne 18
		$sheet_proc->setCellValue('T1', 'hal-id'                     );    // colonne 19
		$sheet_proc->setCellValue('U1', 'Citation'                   );    // colonne 20
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($proceedings as $proceeding){
			
			$sheet_proc->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($proceeding,$soulignAuteur,$equipes));
			$sheet_proc->setCellValueByColumnAndRow( 1,  $ligne, $proceeding['title_s'][0]);
			$sheet_proc->setCellValueByColumnAndRow( 2,  $ligne, $proceeding['journalTitle_s']);
			$sheet_proc->setCellValueByColumnAndRow( 3,  $ligne, $proceeding['volume_s']);
			if (isset($proceeding['issue_s']))
				$sheet_proc->setCellValueByColumnAndRow( 4, $ligne, implode(' ',$proceeding['issue_s']) );
			$sheet_proc->setCellValueByColumnAndRow( 5,  $ligne, $proceeding['page_s'] );
			$sheet_proc->setCellValueByColumnAndRow( 6,  $ligne, getAnnee($proceeding));
			$sheet_proc->setCellValueByColumnAndRow( 7,  $ligne, $proceeding['doiId_s'] );
			$sheet_proc->setCellValueByColumnAndRow( 8,  $ligne, $proceeding['deptStructAcronym_s'] );
			$sheet_proc->setCellValueByColumnAndRow( 9,  $ligne, "");
			$sheet_proc->setCellValueByColumnAndRow( 10, $ligne, getPremierDernier($proceeding,$equipes) );  // Premier, dernier... O/N  
			$sheet_proc->setCellValueByColumnAndRow( 11, $ligne, getOpenAcess($proceeding)  );   
			$sheet_proc->setCellValueByColumnAndRow( 12, $ligne, getVulgarisation($proceeding) );
			$sheet_proc->setCellValueByColumnAndRow( 13, $ligne, getComite($proceeding) );
			$sheet_proc->setCellValueByColumnAndRow( 14, $ligne, getLangueHceres($proceeding));
			$sheet_proc->setCellValueByColumnAndRow( 15, $ligne, $proceeding['wosId_s'][0] );
			$sheet_proc->setCellValueByColumnAndRow( 16, $ligne, " ".$proceeding['arxivId_s']." ",PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			$sheet_proc->setCellValueByColumnAndRow( 17, $ligne, $proceeding['biorxivId_s'][0] );
			$sheet_proc->setCellValueByColumnAndRow( 18, $ligne, $proceeding['pubmedId_s'] );
			$sheet_proc->setCellValueByColumnAndRow( 19, $ligne, $proceeding['halId_s'] );
			$sheet_proc->setCellValueByColumnAndRow( 20, $ligne, getCitationArticle($proceeding));
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}

////////////////////////////////////////////////////////////////
	// ****************** N° spécial de revue *******************************//
	if (!empty($issues)){
		$sheetn = $workbook->createSheet();
		$sheetn->setTitle('N° spécial de revue');
		$sheetn->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheetn->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheetn->setCellValue('C1', 'Revue'                      );    // colonne 2
		$sheetn->setCellValue('D1', 'Volume'                     );    // colonne 3	
		$sheetn->setCellValue('E1', 'Numéro'                     );    // colonne 4
		$sheetn->setCellValue('F1', 'Pages'                      );    // colonne 5
		$sheetn->setCellValue('G1', 'Année'                      );    // colonne 6
		$sheetn->setCellValue('H1', 'DOI'                        );    // colonne 7
		$sheetn->setCellValue('I1', 'Equipe'                     );    // colonne 8
		$sheetn->setCellValue('J1', 'Doctorant (O/N)'            );    // colonne 9
		$sheetn->setCellValue('K1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'           );    // colonne 10
		$sheetn->setCellValue('L1', 'OA (O/N)'                   );    // colonne 11
		$sheetn->setCellValue('M1', 'Langue du document'         );    // colonne 12
		$sheetn->setCellValue('N1', 'Clé UT (WOS)'               );    // colonne 13
		$sheetn->setCellValue('O1', 'arXiv id'                   );    // colonne 14
		$sheetn->setCellValue('P1', 'BioRxiv id'                 );    // colonne 15
		$sheetn->setCellValue('Q1', 'PubMed id'                  );    // colonne 16
		$sheetn->setCellValue('R1', 'hal-id'                     );    // colonne 17
		$sheetn->setCellValue('S1', 'Citation'                   );    // colonne 18
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($issues as $issue){
			
			$sheetn->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($issue,$soulignAuteur,$equipes));
			$sheetn->setCellValueByColumnAndRow( 1,  $ligne, $issue['title_s'][0]);
			$sheetn->setCellValueByColumnAndRow( 2,  $ligne, $issue['journalTitle_s']);
			$sheetn->setCellValueByColumnAndRow( 3,  $ligne, $issue['volume_s']);
			if (isset($issue['issue_s']))
				$sheetn->setCellValueByColumnAndRow( 4, $ligne, implode(' ',$issue['issue_s']) );
			$sheetn->setCellValueByColumnAndRow( 5,  $ligne, $issue['page_s'] );
			$sheetn->setCellValueByColumnAndRow( 6,  $ligne, getAnnee($issue));
			$sheetn->setCellValueByColumnAndRow( 7,  $ligne, $issue['doiId_s'] );
			$sheetn->setCellValueByColumnAndRow( 8,  $ligne, $issue['deptStructAcronym_s'] );
			$sheetn->setCellValueByColumnAndRow( 10, $ligne, getPremierDernier($issue,$equipes) );  // Premier, dernier... O/N  
			$sheetn->setCellValueByColumnAndRow( 11, $ligne, getOpenAcess($issue)  );   
			$sheetn->setCellValueByColumnAndRow( 12, $ligne, getLangueHceres($issue));
			$sheetn->setCellValueByColumnAndRow( 13, $ligne, $issue['wosId_s'][0] );
			$sheetn->setCellValueByColumnAndRow( 14, $ligne, " ".$issue['arxivId_s']." ",PHPExcel_Style_NumberFormat::FORMAT_TEXT);   // les . ressortent en virgule, tenter le formattage de la cellule
			$sheetn->setCellValueByColumnAndRow( 15, $ligne, $issue['biorxivId_s'][0] );
			$sheetn->setCellValueByColumnAndRow( 16, $ligne, $issue['pubmedId_s'] );
			$sheetn->setCellValueByColumnAndRow( 17, $ligne, $issue['halId_s'] );
			$sheetn->setCellValueByColumnAndRow( 18, $ligne, getCitationArticle($issue));
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
		
	// ***********************************************************//
	// ****************** OUVRAGES *******************************//
	if (!empty($ouvrages)){
		$sheet2 = $workbook->createSheet();
		$sheet2->setTitle('Ouvrage');
		$sheet2->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet2->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet2->setCellValue('C1', 'Pages'                      );    // colonne 2
		$sheet2->setCellValue('D1', 'Année'                      );    // colonne 3	
		$sheet2->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet2->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet2->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet2->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet2->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet2->setCellValue('J1', 'Sous-type Ouvrage'          );    // colonne 9
		$sheet2->setCellValue('K1', 'Vulgarisation (O/N)'        );    // colonne 11
		$sheet2->setCellValue('L1', 'Editeur'                    );    // colonne 12
		$sheet2->setCellValue('M1', 'Langue du document'         );    // colonne 13
		$sheet2->setCellValue('N1', 'Clé UT (WOS)'               );    // colonne 14
		$sheet2->setCellValue('O1', 'hal-id'                     );    // colonne 15
		$sheet2->setCellValue('P1', 'Citation'                   );    // colonne 16
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($ouvrages as $ouvrage){
			$sheet2->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($ouvrage,$soulignAuteur,$equipes));
			$sheet2->setCellValueByColumnAndRow( 1,  $ligne, $ouvrage['title_s'][0]);
			$sheet2->setCellValueByColumnAndRow( 2,  $ligne, $ouvrage['page_s']);
			$sheet2->setCellValueByColumnAndRow( 3,  $ligne, getAnnee($ouvrage));
			$sheet2->setCellValueByColumnAndRow( 4,  $ligne, $ouvrage['doiId_s']  );
			$sheet2->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($ouvrage,$equipes));
			$sheet2->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($ouvrage) );
			$sheet2->setCellValueByColumnAndRow( 9,  $ligne, getSousTypeDoc($ouvrage['docSubType_s']) );
			$sheet2->setCellValueByColumnAndRow( 10, $ligne, getVulgarisation($ouvrage) );  
			$sheet2->setCellValueByColumnAndRow( 11, $ligne, $ouvrage['publisher_s'][0] );  // editeur
			$sheet2->setCellValueByColumnAndRow( 12, $ligne, getLangueHceres($ouvrage)  );
			$sheet2->setCellValueByColumnAndRow( 13, $ligne, $ouvrage['wosId_s'][0] );
			$sheet2->setCellValueByColumnAndRow( 14, $ligne, $ouvrage['halId_s'] );
			$sheet2->setCellValueByColumnAndRow( 15, $ligne, getCitationOuvrage($ouvrage));
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}
	
	// ***********************************************************//
	// ****************** CHAPITRES D'OUVRAGE *******************************//
	if (!empty($chapitres)){
		$sheet3 = $workbook->createSheet();
		$sheet3->setTitle('Chapitre d\'ouvrage');
		$sheet3->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet3->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet3->setCellValue('C1', 'Pages'                      );    // colonne 2
		$sheet3->setCellValue('D1', 'Année'                      );    // colonne 3	
		$sheet3->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet3->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet3->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet3->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet3->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet3->setCellValue('J1', 'Titre de l\'ouvrage'        );    // colonne 9
		$sheet3->setCellValue('K1', 'Vulgarisation (O/N)'        );    // colonne 11
		$sheet3->setCellValue('L1', 'Editeur'                    );    // colonne 12
		$sheet3->setCellValue('M1', 'Langue du document'         );    // colonne 13
		$sheet3->setCellValue('N1', 'Clé UT (WOS)'               );    // colonne 14
		$sheet3->setCellValue('O1', 'hal-id'                     );    // colonne 15
		$sheet3->setCellValue('P1', 'Citation'                   );    // colonne 16
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($chapitres as $chapitre){
			$sheet3->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($chapitre,$soulignAuteur,$equipes));
			$sheet3->setCellValueByColumnAndRow( 1,  $ligne, $chapitre['title_s'][0]);
			$sheet3->setCellValueByColumnAndRow( 2,  $ligne, $chapitre['page_s']);
			$sheet3->setCellValueByColumnAndRow( 3,  $ligne, getAnnee($chapitre));
			$sheet3->setCellValueByColumnAndRow( 4,  $ligne, $chapitre['doiId_s']  );
			$sheet3->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($chapitre,$equipes));
			$sheet3->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($chapitre) );
			$sheet3->setCellValueByColumnAndRow( 9,  $ligne, $chapitre['bookTitle_s']  );
			$sheet3->setCellValueByColumnAndRow( 10, $ligne, getVulgarisation($chapitre) );  
			$sheet3->setCellValueByColumnAndRow( 11, $ligne, $chapitre['publisher_s'][0] );  // editeur
			$sheet3->setCellValueByColumnAndRow( 12, $ligne, getLangueHceres($chapitre)  );
			$sheet3->setCellValueByColumnAndRow( 13, $ligne, $chapitre['wosId_s'][0] );
			$sheet3->setCellValueByColumnAndRow( 14, $ligne, $chapitre['halId_s'] );
			$sheet3->setCellValueByColumnAndRow( 15, $ligne, getCitationChapitreOuvrage($chapitre));
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}
	
	// ****************************************************************//
	// ****************** Article de blog scientifique ************************************//
	if (!empty($blogs)){
		$sheet160 = $workbook->createSheet();
		$sheet160->setTitle('Article de blog scientifique');
		$sheet160->setCellValue('A1', 'Auteur'                    );    // colonne 0
		$sheet160->setCellValue('B1', 'Titre'                     );    // colonne 1
		$sheet160->setCellValue('C1', 'Année'                     );    // colonne 2
		$sheet160->setCellValue('D1', 'DOI'                       );    // colonne 3   
		$sheet160->setCellValue('E1', 'Equipe'                    );    // colonne 4   
		$sheet160->setCellValue('F1', 'Doctorant (O/N)'           );    // colonne 5	
		$sheet160->setCellValue('G1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'            );    // colonne 6
		$sheet160->setCellValue('H1', 'OA (O/N)'                  );    // colonne 7
		$sheet160->setCellValue('I1', 'Nom du blog ou du carnet'  );    // colonne 8
		$sheet160->setCellValue('J1', 'Langue du document'        );    // colonne 9
		$sheet160->setCellValue('K1', 'hal-id'                    );    // colonne 10
		$sheet160->setCellValue('L1', 'Citation'                  );    // colonne 11
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($blogs as $blog){
			$sheet160->setCellValueByColumnAndRow(  0,  $ligne, getAuteursSoulign($blog,$soulignAuteur,$equipes));
			$sheet160->setCellValueByColumnAndRow(  1,  $ligne, $blog['title_s'][0]);
			$sheet160->setCellValueByColumnAndRow(  2,  $ligne, getAnnee($blog));
			$sheet160->setCellValueByColumnAndRow(  3,  $ligne, $blog['doiId_s']);
			$sheet160->setCellValueByColumnAndRow(  6,  $ligne, getPremierDernier($blog,$equipes));
			$sheet160->setCellValueByColumnAndRow(  7,  $ligne, getOpenAcess($blog));
			$sheet160->setCellValueByColumnAndRow(  8,  $ligne, $blog['description_s']);
			$sheet160->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($blog) );
			$sheet160->setCellValueByColumnAndRow( 10,  $ligne, $blog['halId_s'] );
			$sheet160->setCellValueByColumnAndRow( 11,  $ligne, getCitationAutre($blog) );
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ***********************************************************//
	// ****************** Not. Encyclopédie dictionnaire *******************************//
	if (!empty($encyclos)){
		$sheet3f = $workbook->createSheet();
		$sheet3f->setTitle('Not. Encyclopédie dictionnaire');
		$sheet3f->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet3f->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet3f->setCellValue('C1', 'Pages'                      );    // colonne 2
		$sheet3f->setCellValue('D1', 'Année'                      );    // colonne 3	
		$sheet3f->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet3f->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet3f->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet3f->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet3f->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet3f->setCellValue('J1', 'Titre de l\'ouvrage'        );    // colonne 9
		$sheet3f->setCellValue('K1', 'Vulgarisation (O/N)'        );    // colonne 11
		$sheet3f->setCellValue('L1', 'Editeur'                    );    // colonne 12
		$sheet3f->setCellValue('M1', 'Langue du document'         );    // colonne 13
		$sheet3f->setCellValue('N1', 'Clé UT (WOS)'               );    // colonne 14
		$sheet3f->setCellValue('O1', 'hal-id'                     );    // colonne 15
		$sheet3f->setCellValue('P1', 'Citation'                   );    // colonne 16
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($encyclos as $encyclo){
			$sheet3f->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($encyclo,$soulignAuteur,$equipes));
			$sheet3f->setCellValueByColumnAndRow( 1,  $ligne, $encyclo['title_s'][0]);
			$sheet3f->setCellValueByColumnAndRow( 2,  $ligne, $encyclo['page_s']);
			$sheet3f->setCellValueByColumnAndRow( 3,  $ligne, getAnnee($encyclo));
			$sheet3f->setCellValueByColumnAndRow( 4,  $ligne, $encyclo['doiId_s']  );
			$sheet3f->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($encyclo,$equipes));
			$sheet3f->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($encyclo) );
			$sheet3f->setCellValueByColumnAndRow( 9,  $ligne, $encyclo['bookTitle_s']  );
			$sheet3f->setCellValueByColumnAndRow( 10, $ligne, getVulgarisation($encyclo) );  
			$sheet3f->setCellValueByColumnAndRow( 11, $ligne, $encyclo['publisher_s'][0] );  // editeur
			$sheet3f->setCellValueByColumnAndRow( 12, $ligne, getLangueHceres($encyclo)  );
			$sheet3f->setCellValueByColumnAndRow( 13, $ligne, $encyclo['wosId_s'][0] );
			$sheet3f->setCellValueByColumnAndRow( 14, $ligne, $encyclo['halId_s'] );
			$sheet3f->setCellValueByColumnAndRow( 15, $ligne, getCitationChapitreOuvrage($encyclo));
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}
	
	// ****************************************************************//
	// ****************** Traduction ************************************//
	if (!empty($trads)){
		$sheet16 = $workbook->createSheet();
		$sheet16->setTitle('Traduction');
		$sheet16->setCellValue('A1', 'Auteur'                    );    // colonne 0
		$sheet16->setCellValue('B1', 'Titre'                     );    // colonne 1
		$sheet16->setCellValue('C1', 'Année'                     );    // colonne 2
		$sheet16->setCellValue('D1', 'DOI'                       );    // colonne 3   
		$sheet16->setCellValue('E1', 'Equipe'                    );    // colonne 4   
		$sheet16->setCellValue('F1', 'Doctorant (O/N)'           );    // colonne 5	
		$sheet16->setCellValue('G1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'            );    // colonne 6
		$sheet16->setCellValue('H1', 'OA (O/N)'                  );    // colonne 7
		$sheet16->setCellValue('I1', 'Référence bibliographique du document traduit'               );    // colonne 8
		$sheet16->setCellValue('J1', 'Langue du document'        );    // colonne 9
		$sheet16->setCellValue('K1', 'hal-id'                    );    // colonne 10
		$sheet16->setCellValue('L1', 'Citation'                  );    // colonne 11
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($trads as $trad){
			$sheet16->setCellValueByColumnAndRow(  0,  $ligne, getAuteursSoulign($trad,$soulignAuteur,$equipes));
			$sheet16->setCellValueByColumnAndRow(  1,  $ligne, $trad['title_s'][0]);
			$sheet16->setCellValueByColumnAndRow(  2,  $ligne, getAnnee($trad));
			$sheet16->setCellValueByColumnAndRow(  3,  $ligne, $trad['doiId_s']);
			$sheet16->setCellValueByColumnAndRow(  6,  $ligne, getPremierDernier($trad,$equipes));
			$sheet16->setCellValueByColumnAndRow(  7,  $ligne, getOpenAcess($trad));
			$sheet16->setCellValueByColumnAndRow(  8,  $ligne, $trad['description_s']);
			$sheet16->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($trad) );
			$sheet16->setCellValueByColumnAndRow( 10,  $ligne, $trad['halId_s'] );
			$sheet16->setCellValueByColumnAndRow( 11,  $ligne, getCitationAutre($trad) );
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ***********************************************************//
	// ****************** DIRECTION D'OUVRAGE *******************************//
/*	if (!empty($douvs)){
		$sheet4 = $workbook->createSheet();
		$sheet4->setTitle('Direction d\'ouvrage ou d\'actes');
		$sheet4->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet4->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet4->setCellValue('C1', 'Pages'                      );    // colonne 2
		$sheet4->setCellValue('D1', 'Année'                      );    // colonne 3	
		$sheet4->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet4->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet4->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet4->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet4->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet4->setCellValue('J1', 'Sous-type Direction ouvrage ou actes'                );    // colonne 9
		$sheet4->setCellValue('K1', 'Vulgarisation (O/N)'        );    // colonne 11
		$sheet4->setCellValue('L1', 'Titre du congrès'        );    // colonne 12
		$sheet4->setCellValue('M1', 'Ville du congrès'        );    // colonne 13
		$sheet4->setCellValue('N1', 'Pays du congrès'        );    // colonne 14
		$sheet4->setCellValue('O1', 'Date de début du congrès'        );    // colonne 15
		$sheet4->setCellValue('P1', 'Date de fin du congrès'        );    // colonne 16
		$sheet4->setCellValue('Q1', 'Editeur'                    );    // colonne 17
		$sheet4->setCellValue('R1', 'Langue du document'         );    // colonne 18
		$sheet4->setCellValue('S1', 'Clé UT (WOS)'               );    // colonne 19
		$sheet4->setCellValue('T1', 'hal-id'                     );    // colonne 20
		$sheet4->setCellValue('U1', 'Citation'                   );    // colonne 21
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($douvs as $douv){
			$sheet4->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($douv,$soulignAuteur,$equipes));
			$sheet4->setCellValueByColumnAndRow( 1,  $ligne, $douv['title_s'][0]);
			$sheet4->setCellValueByColumnAndRow( 2,  $ligne, $douv['page_s']);
			$sheet4->setCellValueByColumnAndRow( 3,  $ligne, getAnnee($douv));
			$sheet4->setCellValueByColumnAndRow( 4,  $ligne, $douv['doiId_s']  );
			$sheet4->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($douv,$equipes));
			$sheet4->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($douv) );
			$sheet4->setCellValueByColumnAndRow( 9,  $ligne, getSousTypeDoc($douv['docSubType_s']) );
			$sheet4->setCellValueByColumnAndRow( 10, $ligne, getVulgarisation($douv) );  
			$sheet4->setCellValueByColumnAndRow( 11, $ligne, $douv['conferenceTitle_s'] );  // titre du congrés
			$sheet4->setCellValueByColumnAndRow( 12, $ligne, $douv['city_s']  );
			$sheet4->setCellValueByColumnAndRow( 13, $ligne, code_to_country($douv['country_s']) );
			$sheet4->setCellValueByColumnAndRow( 14, $ligne, $douv['conferenceStartDate_s']);
			$sheet4->setCellValueByColumnAndRow( 15, $ligne, $douv['conferenceEndDate_s']);
			$sheet4->setCellValueByColumnAndRow( 16, $ligne, $douv['publisher_s'][0] );
			$sheet4->setCellValueByColumnAndRow( 17, $ligne, getLangueHceres($douv) );
			$sheet4->setCellValueByColumnAndRow( 18, $ligne, $douv['wosId_s'][0] );
			$sheet4->setCellValueByColumnAndRow( 19, $ligne, $douv['halId_s'] );
			$sheet4->setCellValueByColumnAndRow( 20, $ligne, getCitationDirectionOuvrage($douv));
			
			
			$ligne++;
			$nbNoticeTraite++;			
		}
	}
	
	*/
	
	// ****************************************************************//
	// ****************** BREVETS *******************************//
	if (!empty($brevets)){
		$sheet7 = $workbook->createSheet();
		$sheet7->setTitle('Brevet');
		$sheet7->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet7->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet7->setCellValue('C1', 'Année'                      );    // colonne 2
		$sheet7->setCellValue('D1', 'Numéro de brevet'           );    // colonne 3	
		$sheet7->setCellValue('E1', 'Pays'                       );    // colonne 4
		$sheet7->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet7->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet7->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet7->setCellValue('I1', 'hal-id'                     );    // colonne 8
		$sheet7->setCellValue('J1', 'Citation'                   );    // colonne 9
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($brevets as $brevet){
			$sheet7->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($brevet,$soulignAuteur,$equipes));
			$sheet7->setCellValueByColumnAndRow( 1,  $ligne, $brevet['title_s'][0]);
			$sheet7->setCellValueByColumnAndRow( 2,  $ligne, getAnnee($brevet));
			$sheet7->setCellValueByColumnAndRow( 3,  $ligne, $brevet['number_s'][0]);
			$sheet7->setCellValueByColumnAndRow( 4,  $ligne, getLangueHceres($brevet) );
			$sheet7->setCellValueByColumnAndRow( 5,  $ligne, "" );
			$sheet7->setCellValueByColumnAndRow( 6,  $ligne, "" );
			$sheet7->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($brevet,$equipes) );
			$sheet7->setCellValueByColumnAndRow( 8,  $ligne, $brevet['halId_s']  );
			$sheet7->setCellValueByColumnAndRow( 9, $ligne, getCitationBrevet($brevet));  
			
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ****************************************************************//
	// ****************** Autre publication others ************************************//
	if (!empty($others)){
		$sheet162 = $workbook->createSheet();
		$sheet162->setTitle('Autre publication');
		$sheet162->setCellValue('A1', 'Auteur'                    );    // colonne 0
		$sheet162->setCellValue('B1', 'Titre'                     );    // colonne 1
		$sheet162->setCellValue('C1', 'Année'                     );    // colonne 2
		$sheet162->setCellValue('D1', 'DOI'                       );    // colonne 3   
		$sheet162->setCellValue('E1', 'Equipe'                    );    // colonne 4   
		$sheet162->setCellValue('F1', 'Doctorant (O/N)'           );    // colonne 5	
		$sheet162->setCellValue('G1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'            );    // colonne 6
		$sheet162->setCellValue('H1', 'OA (O/N)'                  );    // colonne 7
		$sheet162->setCellValue('I1', 'Description'               );    // colonne 8
		$sheet162->setCellValue('J1', 'Langue du document'        );    // colonne 9
		$sheet162->setCellValue('K1', 'hal-id'                    );    // colonne 10
		$sheet162->setCellValue('L1', 'Citation'                  );    // colonne 11
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($others as $other){
			$sheet162->setCellValueByColumnAndRow(  0,  $ligne, getAuteursSoulign($other,$soulignAuteur,$equipes));
			$sheet162->setCellValueByColumnAndRow(  1,  $ligne, $other['title_s'][0]);
			$sheet162->setCellValueByColumnAndRow(  2,  $ligne, getAnnee($other));
			$sheet162->setCellValueByColumnAndRow(  3,  $ligne, $other['doiId_s']);
			$sheet162->setCellValueByColumnAndRow(  6,  $ligne, getPremierDernier($other,$equipes));
			$sheet162->setCellValueByColumnAndRow(  7,  $ligne, getOpenAcess($other));
			$sheet162->setCellValueByColumnAndRow(  8,  $ligne, $other['description_s']);
			$sheet162->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($other) );
			$sheet162->setCellValueByColumnAndRow( 10,  $ligne, $other['halId_s'] );
			$sheet162->setCellValueByColumnAndRow( 11,  $ligne, getCitationAutre($other) );
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ****************************************************************//
	// ****************** PREPRINT & WORKING PAPER *******************//
	if (!empty($prepublis)){
		$sheet9 = $workbook->createSheet();
		$sheet9->setTitle('Preprint, Working Paper');
		$sheet9->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet9->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet9->setCellValue('C1', 'Pages'                      );    // colonne 2
		$sheet9->setCellValue('D1', 'Année'                      );    // colonne 3	
		$sheet9->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet9->setCellValue('F1', 'arXiv id'                   );    // colonne 5
		$sheet9->setCellValue('G1', 'BioRxiv id'                 );    // colonne 6
		$sheet9->setCellValue('H1', 'Equipe'                     );    // colonne 7
		$sheet9->setCellValue('I1', 'Doctorant (O/N)'            );    // colonne 8
		$sheet9->setCellValue('J1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 9
		$sheet9->setCellValue('K1', 'OA (O/N)'                   );    // colonne 10
		$sheet9->setCellValue('L1', 'Preprint / Working Paper'   );    // colonne 11
		$sheet9->setCellValue('M1', 'Titre du volume ou de la collection' );    // colonne 12
		$sheet9->setCellValue('N1', 'Langue du document'         );    // colonne 13
		$sheet9->setCellValue('O1', 'Clé UT (WOS)'                     );    // colonne 14
		$sheet9->setCellValue('P1', 'hal-id'                     );    // colonne 15
		$sheet9->setCellValue('Q1', 'Citation'                   );    // colonne 16
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($prepublis as $prepubli){
			$sheet9->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($prepubli,$soulignAuteur,$equipes));
			$sheet9->setCellValueByColumnAndRow( 1,  $ligne, $prepubli['title_s'][0]);
			$sheet9->setCellValueByColumnAndRow( 2,  $ligne, $prepubli['page_s']);
			$sheet9->setCellValueByColumnAndRow( 3,  $ligne, getAnnee($prepubli));
			$sheet9->setCellValueByColumnAndRow( 4,  $ligne, $prepubli['doiId_s']);
			$sheet9->setCellValueByColumnAndRow( 5,  $ligne, " ".$prepubli['arxivId_s']." ",PHPExcel_Style_NumberFormat::FORMAT_TEXT);   // les . ressortent en virgule, formattage de la cellule
			$sheet9->setCellValueByColumnAndRow( 6,  $ligne, $prepubli['biorxivId_s'][0] );
			$sheet9->setCellValueByColumnAndRow( 9,  $ligne, getPremierDernier($prepubli,$equipes) );
			$sheet9->setCellValueByColumnAndRow( 10, $ligne, getOpenAcess($prepubli) );
			$sheet9->setCellValueByColumnAndRow( 11, $ligne, getSousTypeDoc($prepubli['docSubType_s'])  );
			$sheet9->setCellValueByColumnAndRow( 12, $ligne, $prepubli['serie_s'][0]  );
			$sheet9->setCellValueByColumnAndRow( 13, $ligne, getLangueHceres($prepubli) );
			$sheet9->setCellValueByColumnAndRow( 14, $ligne, $prepubli['wosId_s'][0]);
			$sheet9->setCellValueByColumnAndRow( 15, $ligne, $prepubli['halId_s']  );
			$sheet9->setCellValueByColumnAndRow( 16, $ligne, getCitationPrepubli($prepubli)  );
									
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
	// ****************************************************************//
	// ****************** RAPPORTS ***********************************//
	if (!empty($rapports)){
		$sheet8 = $workbook->createSheet();
		$sheet8->setTitle('Rapport');
		$sheet8->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet8->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet8->setCellValue('C1', 'Année'                      );    // colonne 2
		$sheet8->setCellValue('D1', 'Pages'                      );    // colonne 3	
		$sheet8->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet8->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet8->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet8->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet8->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet8->setCellValue('J1', 'Sous-type Rapport'          );    // colonne 9
		$sheet8->setCellValue('K1', 'Institution'                );    // colonne 12
		$sheet8->setCellValue('L1', 'Langue du document'         );    // colonne 13
		$sheet8->setCellValue('M1', 'hal-id'                     );    // colonne 14
		$sheet8->setCellValue('N1', 'Citation'                   );    // colonne 15
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($rapports as $rapport){
			$sheet8->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($rapport,$soulignAuteur,$equipes));
			$sheet8->setCellValueByColumnAndRow( 1,  $ligne, $rapport['title_s'][0]);
			$sheet8->setCellValueByColumnAndRow( 2,  $ligne, getAnnee($rapport));
			$sheet8->setCellValueByColumnAndRow( 3,  $ligne, $rapport['page_s']);
			$sheet8->setCellValueByColumnAndRow( 4,  $ligne, $rapport['doiId_s']);
			$sheet8->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($rapport,$equipes) );
			$sheet8->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($rapport) );
			$sheet8->setCellValueByColumnAndRow( 9,  $ligne, getSousTypeDoc($rapport['docSubType_s']) );
			$sheet8->setCellValueByColumnAndRow( 10, $ligne, $rapport['authorityInstitution_s'][0]  );
			$sheet8->setCellValueByColumnAndRow( 11, $ligne, getLangueHceres($rapport)  );
			$sheet8->setCellValueByColumnAndRow( 12, $ligne, $rapport['halId_s']  );
			$sheet8->setCellValueByColumnAndRow( 13, $ligne, getCitationRapport($rapport)  );
									
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	// ****************************************************************//
	// ****************** CHAPITRES RAPPORTS DE RECHERCH ***********************************//
	if (!empty($chapitresRapport)){
		$sheet8a = $workbook->createSheet();
		$sheet8a->setTitle('Chapitre de rapport');
		$sheet8a->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet8a->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet8a->setCellValue('C1', 'Année'                      );    // colonne 2
		$sheet8a->setCellValue('D1', 'Pages'                      );    // colonne 3	
		$sheet8a->setCellValue('E1', 'DOI'                        );    // colonne 4
		$sheet8a->setCellValue('F1', 'Equipe'                     );    // colonne 5
		$sheet8a->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet8a->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet8a->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet8a->setCellValue('J1', 'Sous-type Rapport'          );    // colonne 9
		$sheet8a->setCellValue('K1', 'Titre du rapport de recherche'                );    // colonne 10
		$sheet8a->setCellValue('L1', 'Langue du document'         );    // colonne 11
		$sheet8a->setCellValue('M1', 'hal-id'                     );    // colonne 12
		$sheet8a->setCellValue('N1', 'Citation'                   );    // colonne 13
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($chapitresRapport as $chapitreRapport){
			$sheet8a->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($chapitreRapport,$soulignAuteur,$equipes));
			$sheet8a->setCellValueByColumnAndRow( 1,  $ligne, $chapitreRapport['title_s'][0]);
			$sheet8a->setCellValueByColumnAndRow( 2,  $ligne, getAnnee($chapitreRapport));
			$sheet8a->setCellValueByColumnAndRow( 3,  $ligne, $chapitreRapport['page_s']);
			$sheet8a->setCellValueByColumnAndRow( 4,  $ligne, $chapitreRapport['doiId_s']);
			$sheet8a->setCellValueByColumnAndRow( 7,  $ligne, getPremierDernier($chapitreRapport,$equipes));
			$sheet8a->setCellValueByColumnAndRow( 8,  $ligne, getOpenAcess($chapitreRapport) );
			$sheet8a->setCellValueByColumnAndRow( 9,  $ligne, getSousTypeDoc($rapport['docSubType_s']) );
			$sheet8a->setCellValueByColumnAndRow( 10, $ligne, $chapitreRapport['bookTitle_s']  );
			$sheet8a->setCellValueByColumnAndRow( 11, $ligne, getLangueHceres($chapitreRapport)  );
			$sheet8a->setCellValueByColumnAndRow( 12, $ligne, $chapitreRapport['halId_s']  );
			$sheet8a->setCellValueByColumnAndRow( 13, $ligne, getCitationChapitreRapport($chapitreRapport)  );
									
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
	// ****************************************************************//
	// ****************** THESES ************************************//
	if (!empty($theses)){
		$sheet10 = $workbook->createSheet();
		$sheet10->setTitle('Thèse');
		$sheet10->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet10->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet10->setCellValue('C1', 'Date de soutenance'         );    // colonne 2   
		$sheet10->setCellValue('D1', 'Equipe'                     );    // colonne 3	
		$sheet10->setCellValue('E1', 'OA (O/N)'                   );    // colonne 4
		$sheet10->setCellValue('F1', 'Organisme de délivrance'    );    // colonne 5
		$sheet10->setCellValue('G1', 'Langue du document'         );    // colonne 6
		$sheet10->setCellValue('H1', 'hal-id'                     );    // colonne 7
		$sheet10->setCellValue('I1', 'Citation'                   );    // colonne 8
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($theses as $these){
			$sheet10->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($these,$soulignAuteur,$equipes));
			$sheet10->setCellValueByColumnAndRow( 1,  $ligne, $these['title_s'][0]);
			$sheet10->setCellValueByColumnAndRow( 2,  $ligne, $these['producedDate_s']);
			$sheet10->setCellValueByColumnAndRow( 4,  $ligne, getOpenAcess($these));
			$sheet10->setCellValueByColumnAndRow( 5,  $ligne, $these['authorityInstitution_s'][0] );
			$sheet10->setCellValueByColumnAndRow( 6,  $ligne, getLangueHceres($these));
			$sheet10->setCellValueByColumnAndRow( 7,  $ligne, $these['halId_s']);
			$sheet10->setCellValueByColumnAndRow( 8,  $ligne, getCitationTheseHdrMemoire($these) );
												
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
		
	
	// ****************************************************************//
	// ****************** HDR ************************************//
	if (!empty($hdrs)){
		$sheet11 = $workbook->createSheet();
		$sheet11->setTitle('HDR');
		$sheet11->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet11->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet11->setCellValue('C1', 'Date de soutenance'         );    // colonne 2   
		$sheet11->setCellValue('D1', 'Equipe'                     );    // colonne 3	
		$sheet11->setCellValue('E1', 'OA (O/N)'                   );    // colonne 4
		$sheet11->setCellValue('F1', 'Organisme de délivrance'    );    // colonne 5
		$sheet11->setCellValue('G1', 'Langue du document'         );    // colonne 6
		$sheet11->setCellValue('H1', 'hal-id'                     );    // colonne 7
		$sheet11->setCellValue('I1', 'Citation'                   );    // colonne 8
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($hdrs as $hdr){
			$sheet11->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($hdr,$soulignAuteur,$equipes));
			$sheet11->setCellValueByColumnAndRow( 1,  $ligne, $hdr['title_s'][0]);
			$sheet11->setCellValueByColumnAndRow( 2,  $ligne, $hdr['producedDate_s']);
			$sheet11->setCellValueByColumnAndRow( 4,  $ligne, getOpenAcess($hdr));
			$sheet11->setCellValueByColumnAndRow( 5,  $ligne, $hdr['authorityInstitution_s'][0] );
			$sheet11->setCellValueByColumnAndRow( 6,  $ligne, getLangueHceres($hdr));
			$sheet11->setCellValueByColumnAndRow( 7,  $ligne, $hdr['halId_s']);
			$sheet11->setCellValueByColumnAndRow( 8,  $ligne, getCitationTheseHdrMemoire($hdr) );
												
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
	// ****************************************************************//
	// ****************** MEMOIRES ************************************//
	if (!empty($memoires)){
		$sheet12 = $workbook->createSheet();
		$sheet12->setTitle('Mémoire');
		$sheet12->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet12->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet12->setCellValue('C1', 'Date de soutenance'         );    // colonne 2   
		$sheet12->setCellValue('D1', 'Equipe'                     );    // colonne 3	
		$sheet12->setCellValue('E1', 'OA (O/N)'                   );    // colonne 4
		$sheet12->setCellValue('F1', 'Organisme de délivrance'    );    // colonne 5
		$sheet12->setCellValue('G1', 'Langue du document'         );    // colonne 6
		$sheet12->setCellValue('H1', 'hal-id'                     );    // colonne 7
		$sheet12->setCellValue('I1', 'Citation'                   );    // colonne 8
		
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($memoires as $memoire){
			$sheet12->setCellValueByColumnAndRow( 0,  $ligne, getAuteursSoulign($memoire,$soulignAuteur,$equipes));
			$sheet12->setCellValueByColumnAndRow( 1,  $ligne, $memoire['title_s'][0]);
			$sheet12->setCellValueByColumnAndRow( 2,  $ligne, $memoire['producedDate_s']);
			$sheet12->setCellValueByColumnAndRow( 4,  $ligne, getOpenAcess($memoire));
			$sheet12->setCellValueByColumnAndRow( 5,  $ligne, $memoire['authorityInstitution_s'][0] );
			$sheet12->setCellValueByColumnAndRow( 6,  $ligne, getLangueHceres($memoire));
			$sheet12->setCellValueByColumnAndRow( 7,  $ligne, $memoire['halId_s']);
			$sheet12->setCellValueByColumnAndRow( 8,  $ligne, getCitationTheseHdrMemoire($memoire) );
												
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
	
	
	// ****************************************************************//
	// ****************** COURS ************************************//
	if (!empty($cours)){
		$sheet13 = $workbook->createSheet();
		$sheet13->setTitle('Cours');
		$sheet13->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet13->setCellValue('B1', 'Titre'                      );    // colonne 1
		$sheet13->setCellValue('C1', 'Année'                      );    // colonne 2   
		$sheet13->setCellValue('D1', 'DOI'                        );    // colonne 3   
		$sheet13->setCellValue('E1', 'Equipe'                     );    // colonne 4	
		$sheet13->setCellValue('F1', 'Doctorant (O/N)'            );    // colonne 5
		$sheet13->setCellValue('G1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 6
		$sheet13->setCellValue('H1', 'OA (O/N)'                   );    // colonne 7
		$sheet13->setCellValue('I1', 'Nom du cours'               );    // colonne 9
		$sheet13->setCellValue('J1', 'Niveau du cours'            );    // colonne 10
		$sheet13->setCellValue('K1', 'Etablissement qui délivre le cours'    );    // colonne 11
		$sheet13->setCellValue('L1', 'Langue du document'         );    // colonne 12
		$sheet13->setCellValue('M1', 'hal-id'                     );    // colonne 13
		$sheet13->setCellValue('N1', 'Citation'                   );    // colonne 14
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($cours as $cour){
			$sheet13->setCellValueByColumnAndRow(  0,  $ligne, getAuteursSoulign($cour,$soulignAuteur,$equipes));
			$sheet13->setCellValueByColumnAndRow(  1,  $ligne, $cour['title_s'][0]);
			$sheet13->setCellValueByColumnAndRow(  2,  $ligne, getAnnee($cour));
			$sheet13->setCellValueByColumnAndRow(  3,  $ligne, $cour['doiId_s']);
			$sheet13->setCellValueByColumnAndRow(  6,  $ligne, getPremierDernier($cour,$equipes));
			$sheet13->setCellValueByColumnAndRow(  7,  $ligne, getOpenAcess($cour));
			$sheet13->setCellValueByColumnAndRow(  8,  $ligne, $cour['lectureName_s'] );
			$sheet13->setCellValueByColumnAndRow(  9,  $ligne, getLectureType($cour['lectureType_s']) );
			$sheet13->setCellValueByColumnAndRow( 10,  $ligne, $cour['authorityInstitution_s'][0] );
			$sheet13->setCellValueByColumnAndRow( 11,  $ligne, getLangueHceres($cour) );
			$sheet13->setCellValueByColumnAndRow( 12,  $ligne, $cour['halId_s'] );
			$sheet13->setCellValueByColumnAndRow( 13,  $ligne, getCitationCours($cour) );
												
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
	// ****************************************************************//
	// ****************** Image, son, vidéo, Carte ************************************//
	if (!empty($images) || !empty($sons) || !empty($videos) || !empty($cartes)){
		$sheet15 = $workbook->createSheet();
		$sheet15->setTitle('Image, Son, Vidéo, Carte');
		$sheet15->setCellValue('A1', 'Type de document'           );    // colonne 0
		$sheet15->setCellValue('B1', 'Auteur'                     );    // colonne 1
		$sheet15->setCellValue('C1', 'Titre'                      );    // colonne 2
		$sheet15->setCellValue('D1', 'Année'                      );    // colonne 3   
		$sheet15->setCellValue('E1', 'DOI'                        );    // colonne 4   
		$sheet15->setCellValue('F1', 'Equipe'                     );    // colonne 5	
		$sheet15->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet15->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet15->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet15->setCellValue('J1', 'Langue du document'         );    // colonne 10
		$sheet15->setCellValue('K1', 'hal-id'                     );    // colonne 11
		$sheet15->setCellValue('L1', 'Citation'                   );    // colonne 12
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		if (!empty($images)){
			foreach ($images as $image){
				$sheet15->setCellValueByColumnAndRow(  0,  $ligne, getTypeDoc($image['docType_s']));
				$sheet15->setCellValueByColumnAndRow(  1,  $ligne, getAuteursSoulign($image,$soulignAuteur,$equipes));
				$sheet15->setCellValueByColumnAndRow(  2,  $ligne, $image['title_s'][0]);
				$sheet15->setCellValueByColumnAndRow(  3,  $ligne, getAnnee($image));
				$sheet15->setCellValueByColumnAndRow(  4,  $ligne, $image['doiId_s']);
				$sheet15->setCellValueByColumnAndRow(  7,  $ligne, getPremierDernier($image,$equipes));
				$sheet15->setCellValueByColumnAndRow(  8,  $ligne, getOpenAcess($image));
				$sheet15->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($image) );
				$sheet15->setCellValueByColumnAndRow( 10,  $ligne, $image['halId_s'] );
				$sheet15->setCellValueByColumnAndRow( 11,  $ligne, getCitationImageCarte($image) );
				$ligne++;
				$nbNoticeTraite++;				 
			}
		}
		
		if (!empty($sons)){
			foreach ($sons as $son){
				$sheet15->setCellValueByColumnAndRow(  0,  $ligne, getTypeDoc($son['docType_s']));
				$sheet15->setCellValueByColumnAndRow(  1,  $ligne, getAuteursSoulign($son,$soulignAuteur,$equipes));
				$sheet15->setCellValueByColumnAndRow(  2,  $ligne, $son['title_s'][0]);
				$sheet15->setCellValueByColumnAndRow(  3,  $ligne, getAnnee($son));
				$sheet15->setCellValueByColumnAndRow(  4,  $ligne, $son['doiId_s']);
				$sheet15->setCellValueByColumnAndRow(  7,  $ligne, getPremierDernier($son,$equipes));
				$sheet15->setCellValueByColumnAndRow(  8,  $ligne, getOpenAcess($son));
				$sheet15->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($son) );
				$sheet15->setCellValueByColumnAndRow( 10,  $ligne, $son['halId_s'] );
				$sheet15->setCellValueByColumnAndRow( 11,  $ligne, getCitationSonVideo($son) );
				$ligne++;
				$nbNoticeTraite++;				 
			}
		}
		
		if (!empty($videos)){
			foreach ($videos as $video){
				$sheet15->setCellValueByColumnAndRow(  0,  $ligne, getTypeDoc($video['docType_s']));
				$sheet15->setCellValueByColumnAndRow(  1,  $ligne, getAuteursSoulign($video,$soulignAuteur,$equipes));
				$sheet15->setCellValueByColumnAndRow(  2,  $ligne, $video['title_s'][0]);
				$sheet15->setCellValueByColumnAndRow(  3,  $ligne, getAnnee($video));
				$sheet15->setCellValueByColumnAndRow(  4,  $ligne, $video['doiId_s']);
				$sheet15->setCellValueByColumnAndRow(  7,  $ligne, getPremierDernier($video,$equipes));
				$sheet15->setCellValueByColumnAndRow(  8,  $ligne, getOpenAcess($video));
				$sheet15->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($video) );
				$sheet15->setCellValueByColumnAndRow( 10,  $ligne, $video['halId_s'] );
				$sheet15->setCellValueByColumnAndRow( 11,  $ligne, getCitationSonVideo($video) );
				$ligne++;
				$nbNoticeTraite++;				 
			}
		}
		
		if (!empty($cartes)){
			foreach ($cartes as $carte){
				$sheet15->setCellValueByColumnAndRow(  0,  $ligne, getTypeDoc($carte['docType_s']));
				$sheet15->setCellValueByColumnAndRow(  1,  $ligne, getAuteursSoulign($carte,$soulignAuteur,$equipes));
				$sheet15->setCellValueByColumnAndRow(  2,  $ligne, $carte['title_s'][0]);
				$sheet15->setCellValueByColumnAndRow(  3,  $ligne, getAnnee($carte));
				$sheet15->setCellValueByColumnAndRow(  4,  $ligne, $carte['doiId_s']);
				$sheet15->setCellValueByColumnAndRow(  7,  $ligne, getPremierDernier($carte,$equipes));
				$sheet15->setCellValueByColumnAndRow(  8,  $ligne, getOpenAcess($carte));
				$sheet15->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($carte) );
				$sheet15->setCellValueByColumnAndRow( 10,  $ligne, $carte['halId_s'] );
				$sheet15->setCellValueByColumnAndRow( 11,  $ligne, getCitationImageCarte($carte) );
				$ligne++;
				$nbNoticeTraite++;				 
			}
		}		
	}
	
	
	// ****************************************************************//
	// ****************** Logiciels ************************************//
	if (!empty($logs)){
		$sheet14 = $workbook->createSheet();
		$sheet14->setTitle('Logiciel');
		$sheet14->setCellValue('A1', 'Auteur'                     );    // colonne 0
		$sheet14->setCellValue('B1', 'Nom'                        );    // colonne 1
		$sheet14->setCellValue('C1', 'Année'                      );    // colonne 2   
		$sheet14->setCellValue('D1', 'DOI'                        );    // colonne 3   
		$sheet14->setCellValue('E1', 'Equipe'                     );    // colonne 4	
		$sheet14->setCellValue('F1', 'Doctorant (O/N)'            );    // colonne 5
		$sheet14->setCellValue('G1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 6
		$sheet14->setCellValue('H1', 'OA (O/N)'                   );    // colonne 7
		$sheet14->setCellValue('I1', 'hal-id'                     );    // colonne 8
		$sheet14->setCellValue('J1', 'Citation'                   );    // colonne 9
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($logs as $log){
			$sheet14->setCellValueByColumnAndRow(  0,  $ligne, getAuteursSoulign($log,$soulignAuteur,$equipes));
			$sheet14->setCellValueByColumnAndRow(  1,  $ligne, $log['title_s'][0]);
			$sheet14->setCellValueByColumnAndRow(  2,  $ligne, getAnnee($log));
			$sheet14->setCellValueByColumnAndRow(  3,  $ligne, $log['doiId_s']);
			$sheet14->setCellValueByColumnAndRow(  6,  $ligne, getPremierDernier($log,$equipes));
			$sheet14->setCellValueByColumnAndRow(  7,  $ligne, getOpenAcess($log));
			$sheet14->setCellValueByColumnAndRow(  8,  $ligne, $log['halId_s'] );
			$sheet14->setCellValueByColumnAndRow(  9,  $ligne, getCitationLogiciel($log) );
			
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
	
	
		
	
	// ***********************************************************************//
	// ****************** Non classé Autres ************************************//		
	if (!empty($autres)){
		$sheet17 = $workbook->createSheet();
		$sheet17->setTitle('Non classé');
		$sheet17->setCellValue('A1', 'Type de document'           );    // colonne 0
		$sheet17->setCellValue('B1', 'Auteur'                     );    // colonne 1
		$sheet17->setCellValue('C1', 'Titre'                      );    // colonne 2
		$sheet17->setCellValue('D1', 'Année'                      );    // colonne 3   
		$sheet17->setCellValue('E1', 'DOI'                        );    // colonne 4   
		$sheet17->setCellValue('F1', 'Equipe'                     );    // colonne 5	
		$sheet17->setCellValue('G1', 'Doctorant (O/N)'            );    // colonne 6
		$sheet17->setCellValue('H1', 'Publication en premier, dernier ou auteur de correspondance (O/N)'                        );    // colonne 7
		$sheet17->setCellValue('I1', 'OA (O/N)'                   );    // colonne 8
		$sheet17->setCellValue('K1', 'Langue du document'         );    // colonne 10
		$sheet17->setCellValue('L1', 'hal-id'                     );    // colonne 11
		$sheet17->setCellValue('M1', 'Citation'                   );    // colonne 12
		
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		
		foreach ($autres as $autre){
			$sheet17->setCellValueByColumnAndRow(  0,  $ligne, getTypeDoc($autre['docType_s']));
			$sheet17->setCellValueByColumnAndRow(  1,  $ligne, getAuteursSoulign($autre,$soulignAuteur,$equipes));
			$sheet17->setCellValueByColumnAndRow(  2,  $ligne, $autre['title_s'][0]);
			$sheet17->setCellValueByColumnAndRow(  3,  $ligne, getAnnee($autre));
			$sheet17->setCellValueByColumnAndRow(  4,  $ligne, $autre['doiId_s']);
			$sheet17->setCellValueByColumnAndRow(  7,  $ligne, getPremierDernier($autre,$equipes));
			$sheet17->setCellValueByColumnAndRow(  8,  $ligne, getOpenAcess($autre));
			$sheet17->setCellValueByColumnAndRow(  9,  $ligne, getLangueHceres($autre) );
			$sheet17->setCellValueByColumnAndRow( 10,  $ligne, $autre['halId_s'] );
			$sheet17->setCellValueByColumnAndRow( 11,  $ligne, getCitationAutre($autre) );
			$ligne++;
			$nbNoticeTraite++;				 
		}
	}
		
	
	$msg = "Résultats de l'export :<br />";
	$msg .= "Nombre de publications traitées : ".$nbNoticeTraite." sur ".count($notices);
	if (count($articles)>0) $msg .= "<br/>Articles : ".count($articles);
	if (count($comms)>0) $msg .= "<br/>Communications : ".count($comms);
	if (count($posters)>0) $msg .= "<br/>Posters : ".count($posters);
	if (count($proceedings)>0) $msg .= "<br/>Proceedings : ".count($proceedings);
	if (count($issues)>0) $msg .= "<br/>N° spécial de revue : ".count($issues);
	if (count($ouvrages)>0) $msg .= "<br/>Ouvrages : ".count($ouvrages);
	if (count($chapitres)>0) $msg .= "<br/>Chapitres d'ouvrage : ".count($chapitres);
	if (count($blogs)>0) $msg .= "<br/>Articles de blog scientifique : ".count($blogs);
	if (count($encyclos)>0) $msg .= "<br/>Not. Encyclopédie dictionnaire : ".count($encyclos);
	if (count($trads)>0) $msg .= "<br/>Traductions : ".count($trads);
	if (count($brevets)>0) $msg .= "<br/>Brevets : ".count($brevets);
	if (count($others)>0) $msg .= "<br/>Autres publications : ".count($others);
	if (count($prepublis)>0) $msg .= "<br/>Preprint, Working Paper : ".count($prepublis);
	if (count($rapports)>0) $msg .= "<br/>Rapports : ".count($rapports);
	if (count($chapitresRapport)>0) $msg .= "<br/>Chapitres de rapport : ".count($chapitresRapport);
	if (count($theses)>0) $msg .= "<br/>Thèses : ".count($theses);
	if (count($hdrs)>0) $msg .= "<br/>HDR : ".count($hdrs);
	if (count($memoires)>0) $msg .= "<br/>Mémoires : ".count($memoires);
	if (count($cours)>0) $msg .= "<br/>Cours : ".count($cours);
	if (count($images)>0) $msg .= "<br/>Images : ".count($images);
	if (count($sons)>0) $msg .= "<br/>Sons : ".count($sons);
	if (count($videos)>0) $msg .= "<br/>Vidéos : ".count($videos);
	if (count($cartes)>0) $msg .= "<br/>Cartes : ".count($cartes);
	if (count($logs)>0) $msg .= "<br/>Logiciels : ".count($logs);
	if (count($autres)>0) $msg .= "<br/>Non classés : ".count($autres);
	
	
	
	$myXls = uniqid($collection.'_') . '.xlsx';
	$writer = new PHPExcel_Writer_Excel2007($workbook);
	$writer->setOffice2003Compatibility(true);
	// enregistrement du fichier xlsx
	$writer->save($dir . '/xlsx/' . $myXls);

	$urlFile = "/hceres/xlsx/".$myXls;
	//$linkFile = '<a href="'. $urlFile. '" target="_blank">Télécharger ma liste de publications</a>';    a placer dans  retourne[0] pour avoir un lien sous les boutons
	
	
	$retourne[0] =  "<div style=\"height:5vh;\" />" .$linkInrae . "<br />" . $linkHAL . "</div>";
    $retourne[1] =  $msg;
	$retourne[2] =  $urlFile;
	
	if ($debug) {
		foreach($articles as $article)
			fwrite($h,print_r($article,true));
	}
	fclose($h);
	
	echo json_encode( $retourne );
}	
	
?>